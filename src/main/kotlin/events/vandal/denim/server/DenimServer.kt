package events.vandal.denim.server

import events.vandal.denim.Denim
import events.vandal.denim.boss.DragonBattleManager
import events.vandal.denim.boss.fake.FakeDragonBattleManager
import events.vandal.denim.boss.phase.HomingSweepManager
import net.fabricmc.api.DedicatedServerModInitializer
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback
import net.minecraft.command.argument.GameProfileArgumentType
import net.minecraft.entity.EntityType
import net.minecraft.entity.boss.dragon.phase.ChargingPlayerPhase
import net.minecraft.entity.boss.dragon.phase.PhaseType
import net.minecraft.server.MinecraftServer
import net.minecraft.server.command.CommandManager
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Style
import net.minecraft.text.Text
import net.minecraft.text.TextColor
import net.minecraft.text.Texts
import net.minecraft.util.Formatting
import net.minecraft.util.hit.EntityHitResult
import net.minecraft.util.hit.HitResult
import net.minecraft.world.World

open class DenimServer : DedicatedServerModInitializer {
    override fun onInitializeServer() {
        // This is stupid long, but I can't care enough to actually work on actual classes for these

        CommandRegistrationCallback.EVENT.register { dispatcher, _ ->
            dispatcher.register(CommandManager.literal("teststart").requires {
                it.entity == null || (it.entity is ServerPlayerEntity && (it.entity as ServerPlayerEntity).hasPermissionLevel(2))
            }.executes {
                try {
                    FakeDragonBattleManager.start()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                1
            })

            dispatcher.register(CommandManager.literal("reset").requires {
                    it.entity == null || (it.entity is ServerPlayerEntity && (it.entity as ServerPlayerEntity).hasPermissionLevel(2))
            }.executes {
                FakeDragonBattleManager.reset()
                DragonBattleManager.reset()
                it.source.sendMessage(Text.of("Successfully reset the dragon battle."))

                1
            })

            dispatcher.register(CommandManager.literal("testhomingsweep").requires {
                it.entity == null || (it.entity is ServerPlayerEntity && (it.entity as ServerPlayerEntity).hasPermissionLevel(2))
            }.executes {
                if (DragonBattleManager.dragon == null) {
                    it.source.sendError(Text.literal("Dragon not initialized yet!"))

                    return@executes 1
                }

                DragonBattleManager.lastHomingDivePhaseTime = 0
                DragonBattleManager.isRepeatingHomingDive = true
                DragonBattleManager.isHomingDive = true
                DragonBattleManager.dragon?.phaseManager?.setPhase(PhaseType.CHARGING_PLAYER)

                val current = (DragonBattleManager.dragon?.phaseManager?.current) as ChargingPlayerPhase
                HomingSweepManager.selectedTarget = endWorld.players.random() as ServerPlayerEntity
                current.pathTarget = HomingSweepManager.selectedTarget!!.pos.add(0.0, -2.0, 0.0)
                
                it.source.sendMessage(Text.of("Running homing sweep."))

                1
            })

            dispatcher.register(CommandManager.literal("testfocusbeam").requires {
                it.entity == null || (it.entity is ServerPlayerEntity && (it.entity as ServerPlayerEntity).hasPermissionLevel(2))
            }.executes {
                if (DragonBattleManager.dragon == null) {
                    it.source.sendError(Text.literal("Dragon not initialized yet!"))

                    return@executes 1
                }

                DragonBattleManager.dragon?.phaseManager?.setPhase(Denim.MOVE_TO_SPIKE_PHASE)

                it.source.sendMessage(Text.of("Running focus beam."))

                1
            })

            dispatcher.register(CommandManager.literal("stats").requires {
                it.entity?.type == EntityType.PLAYER
            }.executes {
                try {
                    val player = it.source.player!!
                    val metrics = Denim.metrics.getMetric(player.gameProfile)
                    println(player)
                    println(metrics)

                    it.source.sendMessage(
                        Texts.join(
                            listOf(
                                Text.of(
                                    "Stats of ${player.name.string} in ${Denim.metrics.eventName}"
                                ),
                                Text.of(""),
                                Texts.join(
                                    listOf(
                                        Text.of("- Crystals Destroyed:"),
                                        Text.literal("${metrics.crystalsDestroyed}").setStyle(
                                            Style.EMPTY.withColor(if (metrics.crystalsDestroyed > 0) TextColor.fromFormatting(Formatting.GREEN) else TextColor.fromFormatting(Formatting.RED))
                                        )
                                    ), Text.of(" ")
                                ),

                                Texts.join(
                                    listOf(
                                        Text.of("- Total Damage Dealt to Dragon:"),
                                        Text.literal("${metrics.totalDragonDamage}").setStyle(
                                            Style.EMPTY.withColor(if (metrics.totalDragonDamage > 0) TextColor.fromFormatting(Formatting.GREEN) else TextColor.fromFormatting(Formatting.RED))
                                        )
                                    ), Text.of(" ")
                                ),

                                Texts.join(
                                    listOf(
                                        Text.of("- Deaths:"), Text.literal("${metrics.deaths}").setStyle(
                                            Style.EMPTY.withColor(if (metrics.deaths >= 5) TextColor.fromFormatting(Formatting.RED) else TextColor.fromFormatting(Formatting.GREEN))
                                        )
                                    ), Text.of(" ")
                                ),

                                Texts.join(
                                    listOf(
                                        Text.of("- Endermen Killed:"),
                                        Text.literal("${metrics.enderManKills}").setStyle(
                                            Style.EMPTY.withColor(if (metrics.enderManKills > 0) TextColor.fromFormatting(Formatting.GREEN) else TextColor.fromFormatting(Formatting.RED))
                                        )
                                    ), Text.of(" ")
                                ),

                                Texts.join(
                                    listOf(
                                        Text.of("- Arrows Fired:"),
                                        Text.literal("${metrics.totalArrowsShot}").setStyle(
                                            Style.EMPTY.withColor(if (metrics.totalArrowsShot > 0) TextColor.fromFormatting(Formatting.GREEN) else TextColor.fromFormatting(Formatting.RED))
                                        )
                                    ), Text.of(" ")
                                ),

                                Texts.join(
                                    listOf(
                                        Text.of("- Damage Taken:"),
                                        Text.literal("${metrics.totalDamageTaken}").setStyle(
                                            Style.EMPTY.withColor(TextColor.fromFormatting(Formatting.RED))
                                        )
                                    ), Text.of(" ")
                                ),

                                Texts.join(
                                    listOf(
                                        Text.of("- Total Damage Dealt to All Crystals:"),
                                        Text.literal("${metrics.totalCrystalDamage}").setStyle(
                                            Style.EMPTY.withColor(TextColor.fromFormatting(Formatting.RED))
                                        )
                                    ), Text.of(" ")
                                ),

                                Texts.join(
                                    listOf(
                                        Text.of("- Total Enderoids Knocked:"),
                                        Text.literal("${metrics.enderoidsKnocked}").setStyle(
                                            Style.EMPTY.withColor(TextColor.fromFormatting(Formatting.RED))
                                        )
                                    ), Text.of(" ")
                                ),

                                Texts.join(
                                    listOf(
                                        Text.of("- Total Enderoid Drop Attempts:"),
                                        Text.literal("${metrics.carpalTunnel}").setStyle(
                                            Style.EMPTY.withColor(TextColor.fromFormatting(Formatting.RED))
                                        )
                                    ), Text.of(" ")
                                ),
                            ),
                            Text.of("\n")
                        )
                    )
                } catch (e: Exception) {
                    e.printStackTrace()
                    it.source.sendError(Text.literal(e.stackTraceToString()))
                }

                1
            })

            dispatcher.register(CommandManager.literal("forcesave").executes {
                Denim.metrics.save()
                it.source.sendMessage(Text.literal("Force saved metrics"))
                1
            })

            dispatcher.register(CommandManager.literal("spawnenderoid").requires {
                it.entity?.type == EntityType.PLAYER && it.hasPermissionLevel(2)
            }.executes {
                val enderoid = DragonBattleManager.spawnEnderoid(it.source.position, it.source.world)
                if (enderoid == null) {
                    it.source.sendError(Text.literal("Failed to spawn Enderoid: Unknown reason (ziglinEntity returned null)"))
                    return@executes 0
                }

                it.source.sendMessage(Text.literal("Successfully spawned Enderoid."))

                1
            })

            dispatcher.register(CommandManager.literal("setcrystal").requires {
                it.entity?.type == EntityType.PLAYER && it.hasPermissionLevel(2)
            }.executes {
                val player = it.source.player!!
                val hitResult = player.raycast(5.0, 1F, false)

                if (hitResult.type == HitResult.Type.ENTITY && (hitResult as EntityHitResult).entity.type == EntityType.END_CRYSTAL) {

                }

                1
            })

            dispatcher.register(CommandManager.literal("stats").requires {
                it.entity?.type == EntityType.PLAYER
            }.then(CommandManager.argument("player", GameProfileArgumentType.gameProfile())
                .executes {
                    try {
                        val metrics = Denim.metrics.getMetric(it.getArgument("player", GameProfileArgumentType.SelectorBacked::class.java).getNames(it.source).first())

                        it.source.sendMessage(
                            Texts.join(
                                listOf(
                                    Text.of(
                                        "Stats of ${
                                            it.getArgument(
                                                "player",
                                                GameProfileArgumentType.SelectorBacked::class.java
                                            ).getNames(it.source).first().name
                                        } in ${Denim.metrics.eventName}"
                                    ),
                                    Text.of(""),
                                    Texts.join(
                                        listOf(
                                            Text.of("- Crystals Destroyed:"),
                                            Text.literal("${metrics.crystalsDestroyed}").setStyle(
                                                Style.EMPTY.withColor(if (metrics.crystalsDestroyed > 0) TextColor.fromFormatting(Formatting.GREEN) else TextColor.fromFormatting(Formatting.RED))
                                            )
                                        ), Text.of(" ")
                                    ),

                                    Texts.join(
                                        listOf(
                                            Text.of("- Total Damage Dealt to Dragon:"),
                                            Text.literal("${metrics.totalDragonDamage}").setStyle(
                                                Style.EMPTY.withColor(if (metrics.totalDragonDamage > 0) TextColor.fromFormatting(Formatting.GREEN) else TextColor.fromFormatting(Formatting.RED))
                                            )
                                        ), Text.of(" ")
                                    ),

                                    Texts.join(
                                        listOf(
                                            Text.of("- Deaths:"), Text.literal("${metrics.deaths}").setStyle(
                                                Style.EMPTY.withColor(if (metrics.deaths >= 5) TextColor.fromFormatting(Formatting.RED) else TextColor.fromFormatting(Formatting.GREEN))
                                            )
                                        ), Text.of(" ")
                                    ),

                                    Texts.join(
                                        listOf(
                                            Text.of("- Endermen Killed:"),
                                            Text.literal("${metrics.enderManKills}").setStyle(
                                                Style.EMPTY.withColor(if (metrics.enderManKills > 0) TextColor.fromFormatting(Formatting.GREEN) else TextColor.fromFormatting(Formatting.RED))
                                            )
                                        ), Text.of(" ")
                                    ),

                                    Texts.join(
                                        listOf(
                                            Text.of("- Arrows Fired:"),
                                            Text.literal("${metrics.totalArrowsShot}").setStyle(
                                                Style.EMPTY.withColor(if (metrics.totalArrowsShot > 0) TextColor.fromFormatting(Formatting.GREEN) else TextColor.fromFormatting(Formatting.RED))
                                            )
                                        ), Text.of(" ")
                                    ),

                                    Texts.join(
                                        listOf(
                                            Text.of("- Damage Taken:"),
                                            Text.literal("${metrics.totalDamageTaken}").setStyle(
                                                Style.EMPTY.withColor(TextColor.fromFormatting(Formatting.RED))
                                            )
                                        ), Text.of(" ")
                                    ),

                                    Texts.join(
                                        listOf(
                                            Text.of("- Total Damage Dealt to All Crystals:"),
                                            Text.literal("${metrics.totalCrystalDamage}").setStyle(
                                                Style.EMPTY.withColor(TextColor.fromFormatting(Formatting.RED))
                                            )
                                        ), Text.of(" ")
                                    ),
                                ),
                                Text.of("\n")
                            )
                        )
                    } catch (e: Exception) {
                        e.printStackTrace()
                        it.source.sendError(Text.literal(e.stackTraceToString()))
                    }

                    1
                }))
        }
    }

    companion object {
        lateinit var server: MinecraftServer
        lateinit var endWorld: World
    }
}