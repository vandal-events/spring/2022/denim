package events.vandal.denim.server

import java.nio.file.Path

interface SaveDirAccessor {
    val saveDir: Path
}