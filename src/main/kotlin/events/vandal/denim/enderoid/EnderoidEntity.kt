package events.vandal.denim.enderoid

import events.vandal.denim.Denim
import events.vandal.denim.boss.DragonBattleManager
import events.vandal.denim.server.DenimServer
import net.minecraft.entity.EntityType
import net.minecraft.entity.EquipmentSlot
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.damage.DamageSource
import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.entity.effect.StatusEffects
import net.minecraft.entity.mob.Angerable
import net.minecraft.entity.mob.EndermanEntity
import net.minecraft.entity.mob.ZombifiedPiglinEntity
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtList
import net.minecraft.nbt.NbtString
import net.minecraft.network.packet.s2c.play.EntityVelocityUpdateS2CPacket
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvents
import net.minecraft.text.Text
import net.minecraft.text.Texts
import net.minecraft.util.Formatting
import net.minecraft.util.math.Box
import net.minecraft.util.math.Vec3d
import java.util.*
import kotlin.random.Random

// We are using this trick so players aren't required to install a mod for everything to work, only a resource pack.
class EnderoidEntity(
    var baseEntity: ZombifiedPiglinEntity,
    // This is gonna be helpful for identifying individual Enderoids on world reload
    val uuid: UUID = UUID.randomUUID()
) {
    companion object {
        const val VOLUME = 0.4F
    }

    var currentModel: EnderoidModels = EnderoidModels.STANDING
        set(value) {
            field = value
            baseEntity.equipStack(EquipmentSlot.HEAD, ItemStack(Items.CARVED_PUMPKIN).apply {
                this.orCreateNbt.putInt("CustomModelData", value.ordinal)
                this.orCreateNbt.putUuid("enderoidUuid", uuid)
                this.setCustomName(Text.translatable("entity.enderoid.name").styled {
                    it.withItalic(false)
                })
            })
            baseEntity.setEquipmentDropChance(EquipmentSlot.HEAD, 0F)
        }

    private var walkAnimationTicks = 0
    private var jawAnimationTicks = 0
    private var lastMountTick = 0
    private var attackTime = 35
    var isMounted = false
    private var lastPos = Vec3d.ZERO
    var mountedPlayer: ServerPlayerEntity? = null
    var mountedPlayerUUID: UUID? = null

    private var dropAttempts = 0
    private var dropToughness = Random.nextInt(20, 60)

    private var didPlayerDisconnect = false

    init {
        (baseEntity as EnderoidEntityDuck).isEnderoid = true
        (baseEntity as EnderoidEntityDuck).enderoidEntity = this
        // Apparently this is fucking backwards???
        baseEntity.readCustomDataFromNbt(NbtCompound().apply {
            putBoolean("isEnderoid", true)
            putUuid("enderoidUuid", uuid)
        })
        baseEntity.isBaby = true
        baseEntity.customName = Text.translatable("entity.enderoid.name")
        baseEntity.isInvisible = true
        baseEntity.isSilent = true // Do NOT have Zombified Piglin sounds play.
        baseEntity.setEquipmentDropChance(EquipmentSlot.HEAD, 0F)
        baseEntity.addStatusEffect(StatusEffectInstance(StatusEffects.INVISIBILITY, 100000000, 255, false, false))
        baseEntity.addStatusEffect(StatusEffectInstance(StatusEffects.STRENGTH, 100000000, 2, false, false))
        baseEntity.getEquippedStack(EquipmentSlot.MAINHAND).count = 0
        currentModel = EnderoidModels.STANDING
    }

    fun tick() {
        if (baseEntity.isAlive) {
            if (Random.nextInt(1000) == baseEntity.ambientSoundChance++) {
                baseEntity.ambientSoundChance = -baseEntity.minAmbientSoundDelay
                DragonBattleManager.playSound("entity.enderoid.ambient", pos = baseEntity.pos, world = baseEntity.world, category = SoundCategory.HOSTILE, volume = VOLUME)
            }

            if (isMounted) {
                if (mountedPlayer?.isCreative == true || mountedPlayer?.isSpectator == true) {
                    endPlayerMount(mountedPlayer!!.pos)
                } else tickMounted()
            }

            // Regular anger thing
            if (baseEntity.angryAt == null && !isMounted) {
                if (Random.nextInt(5) == 0) {
                    val player = baseEntity.world.getClosestPlayer(baseEntity, 35.0)
                    //println("did this even get a player $player")
                    if (player != null && !player.isInvulnerable && !player.isCreative && !player.isSpectator) {
                        baseEntity.shouldAngerAt(player)
                        baseEntity.angryAt = player.uuid
                    }
                }
            } else if (!isMounted) {
                val playerAngryAt = baseEntity.world.getPlayerByUuid(baseEntity.angryAt)

                // If the Enderoid is too far away, just give up
                if (playerAngryAt == null || playerAngryAt.squaredDistanceTo(baseEntity) >= 75 || playerAngryAt.isCreative || playerAngryAt.isSpectator) {
                    baseEntity.stopAnger()
                    baseEntity.angryAt = null
                    attackTime = 35
                } else {
                    attackTime--

                    // Enderoid Mount
                    if (playerAngryAt.squaredDistanceTo(baseEntity) <= 5 && !baseEntity.isDead) {
                        if (attackTime <= 0) {
                            attackTime = 35
 
                            if (
                                playerAngryAt.getEquippedStack(EquipmentSlot.HEAD).nbt?.contains("enderoidUuid") != true &&
                                Random.nextInt(10) == 0 &&  // Head mount chance: 1/10
                                // Make sure the Enderoid doesn't mount again immediately after.
                                DenimServer.server.ticks - lastMountTick >= 250L
                            ) {
                                initPlayerMount(playerAngryAt as ServerPlayerEntity)
                            } else {
                                baseEntity.tryAttack(playerAngryAt)
                            }
                        }
                    }
                }
            }

            if (lastPos != baseEntity.pos && !isMounted) {
                if (baseEntity.angryAt != null)
                    jawAnimationTicks++

                if (walkAnimationTicks++ == 0) {
                    if (baseEntity.angryAt != null) {
                        when (jawAnimationTicks) {
                            0 -> {
                                currentModel = EnderoidModels.WALK_RIGHT_LOOK_UP_22DEG
                            }
                            10 -> {
                                currentModel = EnderoidModels.WALK_RIGHT_LOOK_UP_22DEG_JAW
                            }
                            20 -> {
                                currentModel = EnderoidModels.WALK_RIGHT_LOOK_UP_22DEG_JAW2
                            }
                            30 -> {
                                currentModel = EnderoidModels.WALK_RIGHT_LOOK_UP_22DEG_JAW
                            }
                            40 -> {
                                currentModel = EnderoidModels.WALK_RIGHT_LOOK_UP_22DEG
                                jawAnimationTicks = 0
                            }
                        }
                    } else {
                        currentModel = EnderoidModels.WALK_RIGHT
                        if (jawAnimationTicks != 0)
                            jawAnimationTicks = 0
                    }
                } else if (walkAnimationTicks == 5) {
                    if (baseEntity.angryAt != null) {
                        when (jawAnimationTicks) {
                            0 -> {
                                currentModel = EnderoidModels.WALK_LEFT_LOOK_UP_22DEG
                            }
                            10 -> {
                                currentModel = EnderoidModels.WALK_LEFT_LOOK_UP_22DEG_JAW
                            }
                            20 -> {
                                currentModel = EnderoidModels.WALK_LEFT_LOOK_UP_22DEG_JAW2
                            }
                            30 -> {
                                currentModel = EnderoidModels.WALK_LEFT_LOOK_UP_22DEG_JAW
                            }
                            40 -> {
                                currentModel = EnderoidModels.WALK_LEFT_LOOK_UP_22DEG
                                jawAnimationTicks = 0
                            }
                        }
                    } else {
                        currentModel = EnderoidModels.WALK_LEFT
                        if (jawAnimationTicks != 0)
                            jawAnimationTicks = 0
                    }
                } else if (walkAnimationTicks >= 9) {
                    walkAnimationTicks = 0
                }
            } else if (walkAnimationTicks != 0 && !isMounted) {
                walkAnimationTicks = 0
                currentModel = EnderoidModels.STANDING

                if (baseEntity.angryAt != null) {
                    if (jawAnimationTicks++ == 0) {
                        currentModel = EnderoidModels.STANDING_LOOK_UP_22DEG
                    } else if (jawAnimationTicks == 10) {
                        currentModel = EnderoidModels.STANDING_LOOK_UP_22DEG_JAW
                    } else if (jawAnimationTicks == 20) {
                        currentModel = EnderoidModels.STANDING_LOOK_UP_22DEG_JAW2
                    } else if (jawAnimationTicks == 30) {
                        currentModel = EnderoidModels.STANDING_LOOK_UP_22DEG_JAW
                    } else if (jawAnimationTicks == 40) {
                        currentModel = EnderoidModels.STANDING_LOOK_UP_22DEG
                        jawAnimationTicks = 0
                    }
                } else {
                    if (jawAnimationTicks != 0)
                        jawAnimationTicks = 0
                }
            }

            lastPos = baseEntity.pos
        }
    }

    fun damage(source: DamageSource) {
        if (currentModel.name.endsWith("JAW") || currentModel.name.endsWith("JAW2")) return

        currentModel = EnderoidModels.values().firstOrNull { it.name == currentModel.name + "_JAW" } ?: return
        Timer().schedule(object : TimerTask() {
            override fun run() {
                if (!currentModel.name.endsWith("JAW")) return

                currentModel = EnderoidModels.values().firstOrNull { it.name == currentModel.name.removeSuffix("JAW") } ?: return
            }
        }, 50L * 5L)

        if (source.attacker?.type == EntityType.PLAYER) {
            this.immediatelyAnger(source.attacker!! as ServerPlayerEntity)

            this.baseEntity.getWorld().getOtherEntities(this.baseEntity, Box.of(this.baseEntity.pos, 10.0, 10.0, 10.0)) {
                (it.type == EntityType.ENDERMAN || (it.type == EntityType.ZOMBIFIED_PIGLIN && (it as EnderoidEntityDuck).isEnderoid)) && it is Angerable
            }.forEach {
                val angerable = (it as Angerable)
                if (angerable.angryAt == null) {
                    // I don't know how to do anger stuff
                    angerable.shouldAngerAt(source.attacker!! as LivingEntity)
                    angerable.angryAt = source.attacker!!.uuid

                    if (angerable is EndermanEntity && angerable.target == null) {
                        // Some won't be angered.
                        if (Random.nextInt(15) != 0) return@forEach

                        angerable.target = source.attacker!! as LivingEntity
                    }
                }
            }
        }
    }

    fun initPlayerMount(player: ServerPlayerEntity) {
        if (player.hasStackEquipped(EquipmentSlot.HEAD)) {
            val helmet = player.getEquippedStack(EquipmentSlot.HEAD)
            player.dropItem(helmet, false)
        }

        baseEntity.isAiDisabled = true
        baseEntity.setNoGravity(true)
        baseEntity.isInvulnerable = true
        // Place the Enderoid entity out of bounds, it'll be reused later.
        baseEntity.teleport(0.0, 5.0, 0.0)

        mountedPlayer = player
        mountedPlayerUUID = player.uuid

        changeMountedEnderoidHeadForPlayer(player, EnderoidModels.MOUNT)
        player.playSound(SoundEvents.BLOCK_CONDUIT_AMBIENT, SoundCategory.HOSTILE, 1F, 1.355F)
        DragonBattleManager.playSound("entity.enderoid.mount", null, SoundCategory.HOSTILE, player.pos, volume = 0.7F)

        DenimServer.endWorld.players.forEach {
            if (player == it)
                return@forEach

            it.sendMessage(Texts.join(listOf(
                Text.of("[!]").copy().formatted(Formatting.YELLOW),
                player.name.copy().formatted(Formatting.LIGHT_PURPLE),
                Text.of("has gotten mounted by an Enderoid! Help them out by knocking the Enderoid off of them!").copy().formatted(Formatting.YELLOW)
            ), Text.of(" ")), false)
        }

        player.sendMessage(Texts.join(listOf(
            Text.of("[!!!]").copy().formatted(Formatting.RED),
            Text.of("You have gotten mounted by an Enderoid!").copy().formatted(Formatting.DARK_PURPLE),
            Text.of("Quick, open your inventory and continuously attempt to drop it from your head, or call for help from your teammates to help punch it off!").copy().formatted(Formatting.LIGHT_PURPLE)
        ), Text.of(" ")), false)

        isMounted = true
    }

    fun endPlayerMount(pos: Vec3d) {
        baseEntity.teleport(pos.x, pos.y, pos.z)
        baseEntity.isAiDisabled = false
        baseEntity.setNoGravity(false)
        baseEntity.isInvulnerable = false

        val timer = Timer()

        // Make sure the player's actually alive
        timer.schedule(object : TimerTask() {
            override fun run() {
                if (mountedPlayer != null && mountedPlayerUUID != null && mountedPlayer!!.isAlive) {
                    mountedPlayer!!.removeStatusEffect(StatusEffects.WEAKNESS)
                    mountedPlayer!!.removeStatusEffect(StatusEffects.MINING_FATIGUE)
                    mountedPlayer!!.removeStatusEffect(StatusEffects.SLOWNESS)

                    mountedPlayer!!.inventory.remove({ it.hasNbt() && it.nbt!!.containsUuid("enderoidUuid") && it.nbt!!.getUuid("enderoidUuid") == this@EnderoidEntity.uuid }, 1, mountedPlayer!!.inventory)

                    mountedPlayer = null
                    mountedPlayerUUID = null

                    timer.cancel()
                }
            }
        }, 0L, 50L)

        isMounted = false
        lastMountTick = DenimServer.server.ticks
    }

    private fun changeMountedEnderoidHeadForPlayer(player: ServerPlayerEntity, model: EnderoidModels) {
        player.inventory.removeOne(player.getEquippedStack(EquipmentSlot.HEAD))
        player.equipStack(EquipmentSlot.HEAD, ItemStack(Items.CARVED_PUMPKIN).apply {
            this.orCreateNbt.putInt("CustomModelData", model.ordinal)
            this.setCustomName(Text.translatable("entity.enderoid.name").styled {
                it.withItalic(false)
            })

            // Thanks to this message:
            // https://canary.discord.com/channels/507304429255393322/507982478276034570/893184319856726048
            // (available in The Fabric Project's Discord)

            val nbt = this.orCreateNbt
            val nbtDisplay = nbt.getCompound(ItemStack.DISPLAY_KEY)
            val nbtLore = NbtList()
            nbtLore.addAll(
                listOf(
                    NbtString.of(
                        Text.Serialization.toJsonTree(
                            Text.translatable("enderoid.mount.lore.1").styled {
                                it.withFormatting(Formatting.RED)
                                it.withItalic(false)
                            }
                        ).toString()
                    ),
                    NbtString.of(
                        Text.Serialization.toJsonTree(
                            Text.translatable("enderoid.mount.lore.2", Text.translatable("key.drop")).styled {
                                it.withFormatting(Formatting.RED)
                                it.withItalic(false)
                            }
                        ).toString()
                    ),
                    NbtString.of(
                        Text.Serialization.toJsonTree(
                            Text.translatable("enderoid.mount.lore.3").styled {
                                it.withFormatting(Formatting.RED)
                                it.withItalic(false)
                            }
                        ).toString()
                    )
                )
            )

            nbtDisplay.put(ItemStack.LORE_KEY, nbtLore)
            nbt.put(ItemStack.DISPLAY_KEY, nbtDisplay)
            nbt.putUuid("enderoidUuid", uuid)

            this.nbt = nbt
        })
    }

    fun tickMounted() {
        if (mountedPlayerUUID == null) {
            endPlayerMount(Vec3d(0.0, 70.0, 0.0))
            return
        }

        // If the player has decided to disconnect, we do a little trolling.
        if (mountedPlayer == null || mountedPlayer!!.isDisconnected) {
            if (DenimServer.server.playerManager.getPlayer(mountedPlayerUUID!!) == null) {
                didPlayerDisconnect = true
            } else {
                mountedPlayer = DenimServer.server.playerManager.getPlayer(mountedPlayerUUID!!)
                didPlayerDisconnect = false
            }
            return
        }

        mountedPlayer!!.addStatusEffect(StatusEffectInstance(StatusEffects.WEAKNESS, 15, 5), baseEntity)
        mountedPlayer!!.addStatusEffect(StatusEffectInstance(StatusEffects.MINING_FATIGUE, 15, 5), baseEntity)
        mountedPlayer!!.addStatusEffect(StatusEffectInstance(StatusEffects.SLOWNESS, 15, 9), baseEntity)

        if (mountedPlayer!!.isDead) {
            endPlayerMount(mountedPlayer!!.pos)
            return
        }

        if (DenimServer.server.ticks % 25 == 0) {
            mountedPlayer!!.damage(
                this.baseEntity.world.damageSources.magic(),
                // We do a little trolling :)
                if (didPlayerDisconnect) 16.5F else 9.5F
            )

            mountedPlayer!!.playSound(SoundEvents.BLOCK_CONDUIT_AMBIENT_SHORT, SoundCategory.HOSTILE, .7325F, 1.655F)
            mountedPlayer!!.playSound(SoundEvents.BLOCK_CONDUIT_AMBIENT_SHORT, SoundCategory.HOSTILE, .7325F, 1.655F)
            DragonBattleManager.playSound("entity.enderoid.mount_attack", pos = mountedPlayer!!.pos, player = mountedPlayer!!, category = SoundCategory.HOSTILE, volume = VOLUME)
        }

        if (DenimServer.server.ticks % 40 == 0 && Random.nextInt(5) == 0) {
            if (mountedPlayer == null)
                return

            DragonBattleManager.playSound("entity.enderoid.whisper", pos = mountedPlayer!!.pos, player = mountedPlayer!!, category = SoundCategory.HOSTILE, volume = VOLUME)
        }

        if (DenimServer.server.ticks % 5 == 0) {
            val player = mountedPlayer!!

            player.teleport(
                player.world as ServerWorld,
                player.pos.x,
                player.pos.y,
                player.z,
                player.yaw + Random.nextDouble(-3.2, 4.2).toFloat(),
                player.pitch + Random.nextDouble(-3.2, 4.2).toFloat()
            )

            player.velocity = player.velocity.add(Vec3d.of(player.movementDirection.vector).multiply(Random.nextDouble(-0.25, 1.1)))
            player.networkHandler.sendPacket(EntityVelocityUpdateS2CPacket(player.id, player.velocity))
        }
    }

    fun immediatelyAnger(player: ServerPlayerEntity) {
        // BLOOOODDD FOR THE BLOOD GODDD
        baseEntity.shouldAngerAt(player)
        baseEntity.angryAt = player.uuid
    }

    fun attemptDrop(): Boolean {
        if (mountedPlayer == null) {
            return true
        }

        val metrics = Denim.metrics.getMetric(mountedPlayer!!.gameProfile)
        metrics.carpalTunnel++

        if (Random.nextInt(100) == 0 || dropAttempts > dropToughness) {
            endPlayerMount(mountedPlayer!!.pos)
            dropAttempts = 0

            baseEntity.damage(baseEntity.world.damageSources.mobAttack(mountedPlayer), (Random.nextFloat() * 2.5F) + 1F)
            return true
        }

        dropAttempts++
        return false
    }
}