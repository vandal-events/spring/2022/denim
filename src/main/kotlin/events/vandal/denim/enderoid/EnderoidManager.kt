package events.vandal.denim.enderoid

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import events.vandal.denim.server.DenimServer
import events.vandal.denim.server.SaveDirAccessor
import net.minecraft.entity.EntityType
import net.minecraft.entity.mob.ZombifiedPiglinEntity
import net.minecraft.server.world.ServerWorld
import org.apache.logging.log4j.LogManager
import java.io.File
import java.util.*

object EnderoidManager {
    val enderoids = mutableListOf<EnderoidEntity>()
    private val logger = LogManager.getLogger(EnderoidManager::class.java)

    init {
        Timer().scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                this@EnderoidManager.save(DenimServer.endWorld as ServerWorld)
            }
        }, 1000L, 35_000L)
    }

    fun addEnderoid(enderoid: EnderoidEntity) {
        enderoids.add(enderoid)
    }

    fun recreateEnderoid(entity: ZombifiedPiglinEntity, uuid: UUID): EnderoidEntity {
        return if (enderoids.any { it.uuid == uuid }) {
            val enderoid = enderoids.first { it.uuid == uuid }

            enderoid.apply {
                this.baseEntity = entity
                this@EnderoidManager.addEnderoid(this)
            }
        } else {
            return EnderoidEntity(entity, uuid).apply {
                this@EnderoidManager.addEnderoid(this)
            }
        }
    }

    fun removeEnderoid(enderoid: EnderoidEntity) {
        enderoids.removeIf { it.uuid == enderoid.uuid }
    }

    fun initWorld(world: ServerWorld) {
        val enderoidData = load(world)

        world.iterateEntities().forEach {
            if (it.type != EntityType.ZOMBIFIED_PIGLIN)
                return@forEach

            if (it is ZombifiedPiglinEntity) {
                if ((it as EnderoidEntityDuck).isEnderoid)
                    // Should be registered in the EnderoidManager.
                    return@forEach

                if (!enderoidData.has(it.uuid.toString()))
                    return@forEach

                enderoids.add(EnderoidEntity(it, UUID.fromString(enderoidData.get(it.uuid.toString()).asString)))
            }
        }
    }

    fun save(world: ServerWorld) {
        val json = JsonObject()

        enderoids.filter { it.baseEntity.world.registryKey == world.registryKey }.forEach {
            json.addProperty(it.baseEntity.uuid.toString(), it.uuid.toString())
        }

        val file = File((world.chunkManager.threadedAnvilChunkStorage as SaveDirAccessor).saveDir.toFile(), "enderoids.json")

        if (!file.exists())
            file.createNewFile()

        file.writeText(Gson().toJson(json))
    }

    fun load(world: ServerWorld): JsonObject {
        val file = File((world.chunkManager.threadedAnvilChunkStorage as SaveDirAccessor).saveDir.toFile(), "enderoids.json")
        if (!file.exists())
            return JsonObject()

        return try {
            JsonParser.parseString(file.readText()).asJsonObject
        } catch (e: Exception) {
            logger.error("Failed to read Enderoid data for ${world.registryKey.value}!")
            e.printStackTrace()

            JsonObject()
        }
    }
}