package events.vandal.denim

//import events.vandal.denim.phase.HomingDivePhase
import events.vandal.denim.boss.phase.MoveToSpikePhase
import events.vandal.denim.metrics.DenimMetricData
import events.vandal.denim.mixin.boss.phase.PhaseTypeAccessor
import events.vandal.denim.boss.phase.RisePhase
import events.vandal.metrics.MetricManager
import net.fabricmc.api.ModInitializer
import net.minecraft.entity.boss.dragon.phase.PhaseType
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

class Denim : ModInitializer {
    override fun onInitialize() {
    }

    companion object {
        val RISING_PHASE: PhaseType<RisePhase> = PhaseTypeAccessor.register(RisePhase::class.java, "Rising")
        //val HOMING_DIVE_PHASE: PhaseType<HomingDivePhase> = PhaseTypeAccessor.register(HomingDivePhase::class.java, "HomingDive")
        val MOVE_TO_SPIKE_PHASE: PhaseType<MoveToSpikePhase> = PhaseTypeAccessor.register(MoveToSpikePhase::class.java, "MoveToSpike")

        val metrics = MetricManager(DenimMetricData::class, "Spring 2022")
        val logger: Logger = LogManager.getLogger("Denim")
    }
}