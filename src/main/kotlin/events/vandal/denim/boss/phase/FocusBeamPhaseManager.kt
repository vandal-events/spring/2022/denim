package events.vandal.denim.boss.phase

import events.vandal.denim.boss.DragonBattleManager
import events.vandal.denim.server.DenimServer
import net.minecraft.entity.boss.dragon.EnderDragonEntity
import net.minecraft.entity.boss.dragon.phase.PhaseType
import net.minecraft.entity.decoration.EndCrystalEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.sound.SoundCategory
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3d
import org.apache.logging.log4j.LogManager
import kotlin.math.cos
import kotlin.math.sin

object FocusBeamPhaseManager {
    // As far as I'm aware, Minecraft does not break with negative entity IDs, and I don't believe anything relies on negative IDs,
    // but just in case, placing the value a little high.
    //private const val fakeCrystalId = -1245

    // This is the value that is supposed to tell the mixin that it is, in fact, a focus beam phase.
    var hasStarted = false
    private var isInitialized = false

    var lastFocusBeam = 0

    var sentTip = false

    private val logger = LogManager.getLogger(FocusBeamPhaseManager::class.java)
    private var ticks = 0
    private var animatingForwards = true
    private var trueTicks = 0

    //private var fakeCrystal: UUID? = null
    private var target: PlayerEntity? = null
    private var crystal: EndCrystalEntity? = null

    //private val playersWithCrystals = mutableListOf<ServerPlayerEntity>()

    fun start() {
        if (isInitialized) {
            logger.warn("FocusBeamPhaseManager initialized whilst there was one already running! Preventing second one.")
            return
        }

        if (DragonBattleManager.dragon == null) {
            logger.warn("FocusBeamPhaseManager started while no dragon was initialized! Preventing start.")
            return
        }

        //fakeCrystal = UUID.randomUUID()

        DragonBattleManager.playSound("ender_dragon.focus_beam_charge", null, SoundCategory.HOSTILE, DragonBattleManager.dragon!!.pos, 100_000F)

        isInitialized = true
        lastFocusBeam = DenimServer.server.ticks
    }

    fun setCrystal(crystal: EndCrystalEntity) {
        this.crystal = crystal
        this.crystal?.isInvulnerable = true
    }

    /*fun removeFakeCrystal(player: ServerPlayerEntity) {
        if (fakeCrystal == null)
            return

        player.networkHandler.sendPacket(EntitiesDestroyS2CPacket(fakeCrystalId))
        playersWithCrystals.remove(player)
    }*/

    /*fun spawnFakeCrystal(player: ServerPlayerEntity, pos: Vec3d) {
        if (fakeCrystal == null)
            return

        if (playersWithCrystals.contains(player))
            return

        removeFakeCrystal(player)
        player.networkHandler.sendPacket(EntitySpawnS2CPacket(
            fakeCrystalId, fakeCrystal, pos.x, pos.y + 1.75, pos.z, 0F, 0F,
            EntityType.END_CRYSTAL, 0, Vec3d.ZERO
        ))

        playersWithCrystals.add(player)
    }*/

    // This should help find players that the dragon can see. I'm not sure how well this will work out,
    // but here we go.
    private fun searchForPlayersInView(dragon: EnderDragonEntity): List<ServerPlayerEntity> {
        val foundPlayers = mutableListOf<ServerPlayerEntity>()

        dragon.getWorld().players.forEach {
            if (it.isSpectator || it.isCreative || it.isInvulnerable)
                return@forEach

            // Modified from https://stackoverflow.com/a/26245931/17841246

            if (it.distanceTo(dragon) > 256)
                return@forEach

            // To be honest with you, I have no idea what the fuck any of this code does.
            val x = (cos(Math.toRadians(dragon.yaw.toDouble())) * cos(Math.toRadians(dragon.pitch.toDouble())))
            val y = (sin(Math.toRadians(dragon.yaw.toDouble())) * cos(Math.toRadians(dragon.pitch.toDouble())))
            val z = sin(Math.toRadians(dragon.pitch.toDouble()))

            val v = Vec3d(x, y, z)

            if (it.pos.negate().dotProduct(v) <= 0)
                return@forEach

            // Modified from https://canary.discord.com/channels/507304429255393322/760651800570757150/803724185607209000 (Fabric Project Discord)

            val traceStep = dragon.pos.subtract(it.pos).normalize().multiply(0.5)
            var currentTrace = it.pos
            var prevTrace: Vec3d?

            for (i in 1..(dragon.pos.subtract(it.pos).length().toInt())) {
                prevTrace = currentTrace
                currentTrace = currentTrace.add(traceStep)

                val ctPos = BlockPos.ofFloored(currentTrace)
                val blockState = dragon.world.getBlockState(ctPos)
                val shape = blockState.getCollisionShape(dragon.world, ctPos)

                if (blockState.isOpaque)
                    return@forEach

                if (
                    !shape.isEmpty && shape.boundingBox.intersects(prevTrace, currentTrace)
                    && ctPos != BlockPos.ofFloored(dragon.pos) && ctPos != BlockPos.ofFloored(it.pos)
                ) {
                    return@forEach
                }
            }

            foundPlayers.add(it as ServerPlayerEntity)
        }

        return foundPlayers
    }

    fun updateCrystalTarget(target: Vec3d) {
        /* buf = PacketByteBufs.create()
            .writeVarInt(fakeCrystalId)

        // Need to make this work one way or another
        DataTracker.entriesToPacket(listOf(
            DataTracker.Entry<Optional<BlockPos>>(EndCrystalEntityAccessor.getBeamTarget(), Optional.of(BlockPos(target.x, target.y, target.z)))
        ), buf)

        // Apparently the Entity Metadata packet under wiki.vg is called EntityTrackerUpdate under Yarn
        player.networkHandler.sendPacket(EntityTrackerUpdateS2CPacket(
            buf
        ))*/

        this.crystal?.beamTarget = BlockPos.ofFloored(target.x, target.y, target.z)
    }

    fun tick() {
        if (DragonBattleManager.dragon == null) {
            logger.warn("Dragon no longer exists, resetting.")
            reset()

            return
        }

        val dragon = DragonBattleManager.dragon!!

        /*dragon.world.players.forEach {
            val player = it as ServerPlayerEntity

            player.networkHandler.sendPacket(EntityS2CPacket.Rotate(dragon.id, dragon.yaw + 2.0, dragon.pitch, dragon.isOnGround))
        }*/

        if (trueTicks % 10 == 0) {
            /*playersWithCrystals.iterator().forEachRemaining {
                if (it.isDisconnected) {
                    playersWithCrystals.remove(it)
                }
            }*/

            // Try to make sure every player has the fake crystal
            /*val playersWithoutCrystals = DenimServer.endWorld.players.filter { !playersWithCrystals.contains(it as ServerPlayerEntity) }

            if (playersWithoutCrystals.isNotEmpty()) {
                playersWithoutCrystals.forEach {
                    //spawnFakeCrystal(it as ServerPlayerEntity, dragon.pos)

                    target?.let { it1 -> updateCrystalTarget(it, it1.pos) }
                }
            }*/
            target?.let { it -> updateCrystalTarget(it.pos) }
        }

        if (ticks < 25)
            ticks++

        // Hopefully rotate the dragon around?
        dragon.yaw += (ticks * 0.15F)
        dragon.yaw = dragon.yaw % 360

        // Focusing beam system

        // This may be rather performance-taxing, let's not have it check too often.
        if (trueTicks % 10 == 0) {
            val playersInView = searchForPlayersInView(dragon).sortedBy { it.distanceTo(dragon) }

            if (playersInView.isNotEmpty()) {
                val unluckyPlayer = playersInView[0]

                dragon.world.players.forEach {
                    //spawnFakeCrystal(it as ServerPlayerEntity, dragon.pos)

                    updateCrystalTarget(unluckyPlayer.pos)
                }

                target = unluckyPlayer
            } else {
                target = null
                /*dragon.world.players.forEach {
                    removeFakeCrystal(it as ServerPlayerEntity)
                }*/
                this.crystal?.beamTarget = BlockPos(0, 40, 0)
                this.crystal?.isInvulnerable = false
            }
        }

        // Once the phase is over
        if (trueTicks >= 400) { // 20 seconds
            DragonBattleManager.dragon!!.phaseManager.setPhase(PhaseType.LANDING)
            /*dragon.world.players.forEach {
                removeFakeCrystal(it as ServerPlayerEntity)
            }*/
            this.crystal?.beamTarget = BlockPos(0, 40, 0)
            this.crystal?.isInvulnerable = false

            return
        }

        trueTicks++

        // Target check.
        if (target != null) {
            if (target!!.isDead) {
                target = null
                /*dragon.world.players.forEach {
                    removeFakeCrystal(it as ServerPlayerEntity)
                }*/
                this.crystal?.beamTarget = BlockPos(0, 40, 0)
                this.crystal?.isInvulnerable = false

                return
            }

            dragon.world.players.forEach {
                //spawnFakeCrystal(it as ServerPlayerEntity, dragon.pos)
                updateCrystalTarget(target!!.pos)
            }

            if (trueTicks % 5 == 0) {
                target!!.damage(target!!.world.damageSources.dragonBreath(), 6.2735F)
            }
        }
    }

    fun load() {
        hasStarted = true
        DragonBattleManager.dragon!!.phaseManager.setPhase(PhaseType.SITTING_ATTACKING)
    }

    fun reset() {
        isInitialized = false
        ticks = 0
        trueTicks = 0
        animatingForwards = true
        hasStarted = false

        /*playersWithCrystals.clear()
        fakeCrystal = null*/
        this.crystal?.beamTarget = BlockPos(0, 40, 0)
        this.crystal?.isInvulnerable = false

        lastFocusBeam = 0
    }
}