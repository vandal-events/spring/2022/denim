package events.vandal.denim.boss.phase

//class HomingDivePhase(dragon: EnderDragonEntity) {
    /*private val logger = LogManager.getLogger("Homing Dive Phase")

    override fun getType(): PhaseType<out Phase> {
        return Denim.HOMING_DIVE_PHASE
    }

    var player: ServerPlayerEntity? = null
    var lastKnownPlayerPos: Vec3d? = null
    var initialized = false
    var runTime = 0
    private var speed = dragon.movementSpeed

    var hasHitPlayer = false

    override fun beginPhase() {
        logger.info("Entering homing dive phase")
        val closestPlayer = dragon.world.getClosestPlayer(TargetPredicate.DEFAULT.setPredicate {
            it.type == EntityType.PLAYER && it.distanceTo(dragon) <= 320
                    && !(it as ServerPlayerEntity).isSpectator && !(it).isCreative
                    && it.isAlive
        }, dragon)

        if (closestPlayer != null) {
            player = closestPlayer as ServerPlayerEntity
        }

        if (player == null ||
            (DenimServer.server.ticks - DragonBattleManager.lastHomingDivePhaseTime <= 300 && !DragonBattleManager.isRepeatingHomingDive)
        ) {
            logger.warn("Could not find player, exiting.")
            if (player != null)
                logger.warn("If player is there, this is the info: ${player?.name?.asString()}")
            logger.info("Server ticks: ${DenimServer.server.ticks} Last homing dive: ${DragonBattleManager.lastHomingDivePhaseTime}")
            DragonBattleManager.isRepeatingHomingDive = false
            dragon.phaseManager.setPhase(PhaseType.HOLDING_PATTERN)
            return
        }

        lastKnownPlayerPos = player?.pos

        // Get the dragon to adjust to the player's Y coordinate

        /*dragon.velocity = dragon.velocity.add(
            Vec3d.of(dragon.movementDirection.vector).multiply(3.2)
        ).add(0.0, if (dragon.y > lastKnownPlayerPos!!.y) -2.7 else 2.7, 0.0)*/

        DragonBattleManager.lastHomingDivePhaseTime = DenimServer.server.ticks
        DragonBattleManager.isRepeatingHomingDive = false
        initialized = true
    }

    override fun serverTick() {
        if (!initialized)
            return

        runTime++

        speed += if (runTime <= 8) {
            0.1F
        } else {
            (dragon.movementSpeed + (0.4F * runTime)) - speed
        }
        
        if (dragon.y - lastKnownPlayerPos!!.y <= 2.0 || lastKnownPlayerPos!!.y - dragon.y <= 2.0) {
            dragon.velocity = Vec3d.ZERO
        }

        val hitEntitiesList = mutableListOf<Entity>()

        hitEntitiesList.addAll(dragon.world.getOtherEntities(dragon, dragon.head.boundingBox.expand(1.0), EntityPredicates.EXCEPT_CREATIVE_OR_SPECTATOR))
        hitEntitiesList.addAll(dragon.world.getOtherEntities(dragon, (dragon as EnderDragonEntityAccessor).neck.boundingBox.expand(1.0), EntityPredicates.EXCEPT_CREATIVE_OR_SPECTATOR))
        hitEntitiesList.addAll(dragon.world.getOtherEntities(dragon, (dragon as EnderDragonEntityAccessor).rightWing.boundingBox.expand(4.0, 2.0, 4.0).offset(0.0, -2.0, 0.0), EntityPredicates.EXCEPT_CREATIVE_OR_SPECTATOR))
        hitEntitiesList.addAll(dragon.world.getOtherEntities(dragon, (dragon as EnderDragonEntityAccessor).leftWing.boundingBox.expand(4.0, 2.0, 4.0).offset(0.0, -2.0, 0.0), EntityPredicates.EXCEPT_CREATIVE_OR_SPECTATOR))

        hitEntitiesList.forEach {
            if (!hasHitPlayer)
                hasHitPlayer = true

            if (it is LivingEntity)
                damage(it)
        }

        if (runTime >= 120) {
            if (!hasHitPlayer) {
                dragon.health -= 15
                logger.info("Did not hit anyone, damaging self")
            }

            logger.info("Exiting phase")
            dragon.phaseManager.setPhase(PhaseType.HOLDING_PATTERN)
        }
    }

    override fun endPhase() {
        player = null
        runTime = 0
        lastKnownPlayerPos = null
        hasHitPlayer = false

        if (Random.nextInt(50) == 0 && initialized) { // 1/50 chance of occurring again, you'd have to be really unlucky to end up getting hit 10 times.
            DragonBattleManager.isRepeatingHomingDive = true
            dragon.phaseManager.setPhase(Denim.HOMING_DIVE_PHASE)
            logger.info("Oh shit here we go again")
        }
        initialized = false
    }

    override fun getPathTarget(): Vec3d? {
        return lastKnownPlayerPos
    }

    fun damage(entity: LivingEntity) {
        if (entity.type == EntityType.END_CRYSTAL)
            return

        logger.info("Hit entity ${entity.type.name.asString()}/${entity.uuid}")

        entity.damage(DamageSource.mob(dragon), speed * 1.2275F)
        entity.velocity = entity.velocity.add(Vec3d.of(dragon.movementDirection.vector).multiply(1.3 * (speed * 0.2)))
        if (entity is ServerPlayerEntity)
            entity.networkHandler.sendPacket(EntityVelocityUpdateS2CPacket(entity.id, entity.velocity))
    }*/
//}