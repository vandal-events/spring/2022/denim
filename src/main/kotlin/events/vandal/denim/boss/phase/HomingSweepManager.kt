package events.vandal.denim.boss.phase

import events.vandal.denim.boss.DragonBattleManager
import events.vandal.denim.server.DenimServer
import net.minecraft.entity.EntityType
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.boss.dragon.phase.ChargingPlayerPhase
import net.minecraft.entity.boss.dragon.phase.PhaseType
import net.minecraft.network.packet.s2c.play.EntityVelocityUpdateS2CPacket
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.util.math.Vec3d
import org.apache.logging.log4j.LogManager
import kotlin.math.abs

object HomingSweepManager {
    private val logger = LogManager.getLogger(HomingSweepManager::class.java)

    var speed = 0.0
    var selectedTarget: ServerPlayerEntity? = null
    var isInitialized = false
    private var ticks = 0
    private var hasHitPlayer = false

    private var oldMovementSpeed = 0F
    private var oldUpwardSpeed = 0F
    private var oldForwardSpeed = 0F

    var value = 0
    const val MAX_VALUE = 21

    var attempts = 0
    var isLookingAtPlayer = false

    fun begin() {
        if (isInitialized) {
            logger.warn("Homing Dive Manager started while one was already running! Preventing second one.")
            return
        }

        if (DragonBattleManager.dragon == null) {
            logger.warn("Homing Dive Manager started while no dragon was initialized! Preventing start.")
            return
        }

        // 1 minute 15 seconds.
        if (DenimServer.server.ticks - DragonBattleManager.lastHomingDivePhaseTime <= 1500) {
            return
        }

        isInitialized = true

        oldMovementSpeed = DragonBattleManager.dragon!!.movementSpeed
        oldUpwardSpeed = DragonBattleManager.dragon!!.upwardSpeed
        oldForwardSpeed = DragonBattleManager.dragon!!.forwardSpeed
    }

    fun tick() {
        if (isInitialized) {
            ticks++

            val dragon = DragonBattleManager.dragon!!

            /*if (selectedTarget != null && DragonBattleManager.dragon!!.distanceTo(selectedTarget!!) < 4 && !hasPassedPlayer) {
                hasPassedPlayer = true
            }*/

            if (Vec3d(dragon.x, 0.0, dragon.z).distanceTo(Vec3d.ZERO) >= 150) {
                dragon.velocity = Vec3d.ZERO // Stop the dragon from going past the unloading area
            }

            if (ticks % 30 == 0) {
                if (value + 1 <= MAX_VALUE)
                    speed = (abs(abs(value++ - 10) - 10).toDouble() / 5) * 10

                dragon.movementSpeed = speed.toFloat()
                dragon.setUpwardSpeed(speed.toFloat())
                dragon.setForwardSpeed(speed.toFloat())

                dragon.velocity.multiply(speed)
            }

            if (value >= MAX_VALUE && !hasHitPlayer) { // Attempt to hit again
                (dragon.phaseManager.current as ChargingPlayerPhase).pathTarget = if (selectedTarget?.isAlive == true && selectedTarget!!.distanceTo(dragon) <= 50)
                    selectedTarget!!.pos.add(0.0, 2.0, 0.0)
                else {
                    val players = DenimServer.endWorld.players.filter { !it.isCreative && !it.isSpectator && !it.isInvulnerable }

                    if (players.isEmpty()) {
                        dragon.phaseManager.setPhase(PhaseType.LANDING_APPROACH)

                        return
                    }

                    selectedTarget = players.random() as ServerPlayerEntity
                    selectedTarget!!.pos.add(0.0, 2.0, 0.0)
                }

                if (ticks > 250) {
                    ticks = 0
                    value = 0
                }
            }

            if (attempts >= 20 || hasHitPlayer) {
                if (hasHitPlayer && speed < 1) { // Not good enough fuck you
                    hasHitPlayer = false
                    attempts = 0
                    ticks = 0
                    value = 0
                    return
                }

                dragon.phaseManager.setPhase(PhaseType.LANDING_APPROACH)
            }
        }
    }

    fun damage(entity: LivingEntity) {
        if (entity.type == EntityType.END_CRYSTAL)
            return

        val multiplier = 8.44735F
        val damage = speed.toFloat() * multiplier + 10F

        logger.info("Speed: $speed")
        logger.info("Damage: $damage")

        entity.damage(entity.world.damageSources.mobAttack(DragonBattleManager.dragon), damage)
        entity.velocity = entity.velocity.add(Vec3d.of(DragonBattleManager.dragon!!.movementDirection.vector).multiply(-2.3 * ((speed * 5) * 0.2 + 1)))
        if (entity is ServerPlayerEntity) {
            entity.networkHandler.sendPacket(EntityVelocityUpdateS2CPacket(entity.id, entity.velocity))
            hasHitPlayer = true
        }
    }

    fun end() {
        speed = 0.0
        ticks = 0
        hasHitPlayer = false
        selectedTarget = null

        isLookingAtPlayer = false

        if (DragonBattleManager.dragon != null) {
            DragonBattleManager.dragon!!.movementSpeed = oldMovementSpeed
            DragonBattleManager.dragon!!.setForwardSpeed(oldForwardSpeed)
            DragonBattleManager.dragon!!.setUpwardSpeed(oldUpwardSpeed)
        }

        oldMovementSpeed = 0F
        oldForwardSpeed = 0F
        oldUpwardSpeed = 0F

        DragonBattleManager.isHomingDive = false

        DragonBattleManager.lastHomingDivePhaseTime = DenimServer.server.ticks

        isInitialized = false

        value = 0
    }
}