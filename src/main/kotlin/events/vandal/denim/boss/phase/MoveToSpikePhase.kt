package events.vandal.denim.boss.phase

import events.vandal.denim.Denim
import events.vandal.denim.boss.CustomEndCrystal
import events.vandal.denim.boss.DragonBattleManager
import events.vandal.denim.server.DenimServer
import net.minecraft.entity.boss.dragon.EnderDragonEntity
import net.minecraft.entity.boss.dragon.phase.AbstractPhase
import net.minecraft.entity.boss.dragon.phase.Phase
import net.minecraft.entity.boss.dragon.phase.PhaseType
import net.minecraft.entity.decoration.EndCrystalEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.sound.SoundCategory
import net.minecraft.text.Text
import net.minecraft.text.Texts
import net.minecraft.util.Formatting
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3d
import net.minecraft.world.Heightmap
import net.minecraft.world.gen.feature.EndSpikeFeature

class MoveToSpikePhase(val dragon: EnderDragonEntity) : AbstractPhase(dragon) {
    private var chosenTowerPoint: Vec3d? = null

    override fun getType(): PhaseType<out Phase> {
        return Denim.MOVE_TO_SPIKE_PHASE
    }

    override fun beginPhase() {
        chosenTowerPoint = getTopPointOfClosestTower()

        if (chosenTowerPoint == null) {
            this.dragon.phaseManager.setPhase(PhaseType.HOLDING_PATTERN)

            return
        }

        if (!FocusBeamPhaseManager.sentTip) {
            DenimServer.endWorld.players.forEach {
                it.sendMessage(
                    Texts.join(
                        listOf(
                            Text.of("[!]").copy().formatted(Formatting.YELLOW),
                            Text.of("The Dragon is preparing its Focus Beam! Quick, hide behind something away from it!")
                                .copy().formatted(
                                Formatting.RED
                            )
                        ), Text.of(" ")
                    ), false
                )

                it.sendMessage(Text.of("(this message will not be sent again, so listen out for the Dragon's warning!)").copy().formatted(Formatting.ITALIC, Formatting.GRAY), false)
            }

            FocusBeamPhaseManager.sentTip = true
        }

        DragonBattleManager.playSound("ender_dragon.focus_beam_warning", null, SoundCategory.HOSTILE, dragon.pos, 1000F)
    }

    override fun getPathTarget(): Vec3d? {
        return if (chosenTowerPoint != null)
            chosenTowerPoint!!
        else
            getTopPointOfClosestTower()
    }

    fun getTopPointOfClosestTower(): Vec3d? {
        val endWorld = DenimServer.endWorld
        val topPoints = mutableMapOf<Vec3d, EndSpikeFeature.Spike>()

        for (spike in EndSpikeFeature.getSpikes(endWorld as ServerWorld)) {
            val topPoint = DenimServer.endWorld.getTopY(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, spike.centerX, spike.centerZ)

            if (endWorld.getNonSpectatingEntities(EndCrystalEntity::class.java, spike.boundingBox).isEmpty())
                continue

            topPoints[Vec3d(spike.centerX.toDouble(), topPoint.toDouble(), spike.centerZ.toDouble())] = spike
        }

        if (topPoints.isEmpty()) {
            dragon.phaseManager.setPhase(PhaseType.HOLDING_PATTERN)

            return null
        }

        val keys = topPoints.keys.toMutableList()
        keys.sortBy {
            it.distanceTo(dragon.pos)
        }

        val crystals = endWorld.getNonSpectatingEntities(EndCrystalEntity::class.java, topPoints[keys[0]]!!.boundingBox)
        FocusBeamPhaseManager.setCrystal(crystals[0])

        // Return closest tower
        return keys[0]
    }

    override fun serverTick() {
        if (chosenTowerPoint != null) {
            val dragonPosAtTopY = Vec3d(dragon.x, chosenTowerPoint!!.y, dragon.z)

            if (Vec3d(0.0, dragon.y, 0.0).distanceTo(Vec3d(0.0, chosenTowerPoint!!.y, 0.0)) >= 7.0)
                return

            if (dragonPosAtTopY.squaredDistanceTo(chosenTowerPoint) <= 8.0) {
                if (dragon.y - chosenTowerPoint!!.y >= 1.5 || dragon.y - chosenTowerPoint!!.y <= -1.5) {
                    // We don't want to wait too long for the dragon to get up there, let's just teleport it.
                    dragon.teleport(chosenTowerPoint!!.x, chosenTowerPoint!!.y, chosenTowerPoint!!.z)
                }

                FocusBeamPhaseManager.load()
            }
        }
    }
}