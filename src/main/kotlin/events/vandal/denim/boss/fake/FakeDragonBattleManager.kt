package events.vandal.denim.boss.fake

import events.vandal.denim.boss.DragonBattleManager
import events.vandal.denim.mixin.EndSpikeFeatureInvoker
import events.vandal.denim.mixin.boss.EnderDragonFightAccessor
import events.vandal.denim.server.DenimServer
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityType
import net.minecraft.entity.SpawnReason
import net.minecraft.entity.boss.BossBar
import net.minecraft.entity.boss.ServerBossBar
import net.minecraft.entity.boss.dragon.EnderDragonEntity
import net.minecraft.entity.boss.dragon.phase.PhaseType
import net.minecraft.entity.decoration.EndCrystalEntity
import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.entity.effect.StatusEffects
import net.minecraft.network.packet.s2c.play.EntityVelocityUpdateS2CPacket
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvents
import net.minecraft.text.HoverEvent
import net.minecraft.text.Text
import net.minecraft.util.Formatting
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3d
import net.minecraft.world.Heightmap
import net.minecraft.world.ServerWorldAccess
import net.minecraft.world.World
import net.minecraft.world.gen.feature.EndSpikeFeature
import net.minecraft.world.gen.feature.EndSpikeFeatureConfig
import java.util.*

object FakeDragonBattleManager {
    var isFake = false
    var triggeredTrueBattle = false
    var dragon: EnderDragonEntity? = null
    val dragonBossBar = ServerBossBar(
        Text.translatable("entity.minecraft.ender_dragon"),
        BossBar.Color.PINK,
        BossBar.Style.PROGRESS
    ).setDragonMusic(true).setThickenFog(true) as ServerBossBar

    private var triggerTicks = 0
    private var messageStage = 0
    private var respawningCrystal: EndCrystalEntity? = null

    private val unfinishedTowers = mutableListOf<EndSpikeFeature.Spike>()
    private val upgradedTowers = mutableListOf<EndSpikeFeature.Spike>()
    private val upgradingTowers = mutableListOf<SpikeStructureBuilder>()

    private var hasItDied = false
    private var triggeredFake = false

    private var shouldSpawnPortalSoon = false
    private var hasPortalSpawned = false
    private var portalSpawnTicks = 0

    fun start() {
        this.reset()
        isFake = true

        dragon = EnderDragonEntity(EntityType.ENDER_DRAGON, DenimServer.endWorld)

        dragonBossBar.isVisible = true
        dragonBossBar.percent = 1.0F

        DenimServer.endWorld.players.forEach {
            dragonBossBar.addPlayer(it as ServerPlayerEntity)
        }

        // Kotlin shut the fuck up
        val dragonDeezNuts = dragon!!

        dragonDeezNuts.health = 50F

        dragonDeezNuts.initialize(
            DenimServer.endWorld as ServerWorldAccess,
            DenimServer.endWorld.getLocalDifficulty(
                DenimServer.endWorld.getTopPosition(Heightmap.Type.MOTION_BLOCKING, BlockPos(0, 15, 0))
            ),
            SpawnReason.TRIGGERED,
            null, null
        )
        dragonDeezNuts.setPos(0.0, 75.0, 0.0)
        dragonDeezNuts.phaseManager.setPhase(PhaseType.HOLDING_PATTERN)

        DenimServer.endWorld.spawnEntity(dragonDeezNuts)

        val dragonFight = (DenimServer.endWorld as ServerWorld).enderDragonFight
        (dragonFight as EnderDragonFightAccessor).invokeGenerateEndPortal(false)

        for (spike in EndSpikeFeature.getSpikes(DenimServer.endWorld as ServerWorld)) {
            unfinishedTowers.add(spike)
        }

        unfinishedTowers.sortBy { it.height }
    }

    fun reset() {
        dragonBossBar.clearPlayers()
        dragon?.remove(Entity.RemovalReason.DISCARDED)
        dragon = null

        triggerTicks = 0
        messageStage = 0
        isFake = false
        triggeredTrueBattle = false
        respawningCrystal?.remove(Entity.RemovalReason.DISCARDED)
        respawningCrystal = null

        upgradedTowers.clear()
        unfinishedTowers.clear()

        upgradingTowers.forEach {
            it.reset()
        }
        upgradingTowers.clear()

        hasItDied = false
        triggeredTrueBattle = false
        triggeredFake = false

        shouldSpawnPortalSoon = false
        hasPortalSpawned = false
        portalSpawnTicks = 0

        val spikes = EndSpikeFeature.getSpikes(DenimServer.endWorld as ServerWorld)
        val spikeConfig = EndSpikeFeatureConfig(false, spikes, null)

        val feature = (EndSpikeFeature.END_SPIKE as EndSpikeFeature) as EndSpikeFeatureInvoker
        val random = net.minecraft.util.math.random.Random.create((DenimServer.endWorld as ServerWorld).seed)

        for (spike in spikes) {
            val list = DenimServer.endWorld.getNonSpectatingEntities(EndCrystalEntity::class.java, spike.boundingBox)
            feature.invokeGenerateSpike(DenimServer.endWorld as ServerWorld, random, spikeConfig, spike)

            list.iterator().forEachRemaining {
                it.remove(Entity.RemovalReason.DISCARDED)
            }
        }

        DenimServer.server.playerManager.playerList.forEach {
            it.setNoGravity(false)
        }
    }

    fun fakeFinish() {
        DenimServer.endWorld.players.forEach {
            it.sendMessage(
                Text.translatable(
                    "chat.type.advancement.task",
                    it.displayName,
                    Text.translatable(
                        "chat.square_brackets",
                        Text.translatable("advancements.end.kill_dragon.title")
                    )
                        .formatted(Formatting.GREEN).styled { style ->
                            style.withHoverEvent(
                                HoverEvent(
                                    HoverEvent.Action.SHOW_TEXT,
                                    Text.of("...or is it?")
                                )
                            )
                        }
                ), false)
        }

        shouldSpawnPortalSoon = true
    }

    fun endFake() { // This gets triggered in EndPortalBlockMixin
        if (triggeredFake)
            return

        triggeredFake = true
        val timer = Timer()

        val dragonFight = (DenimServer.endWorld as ServerWorld).enderDragonFight
        (dragonFight as EnderDragonFightAccessor).invokeGenerateEndPortal(false)

        timer.schedule(object : TimerTask() {
            override fun run() {
                triggeredTrueBattle = true
                isFake = false

                DragonBattleManager.playSound("denim.fake_end", category = SoundCategory.VOICE, pos = Vec3d.ZERO.add(0.0, 60.0, 0.0))
                dragonBossBar.clearPlayers()
            }
        }, 2_500L)
    }

    fun tick() {
        if (dragon != null && (dragon?.health ?: 0F) / 50 != dragonBossBar.percent) {
            dragonBossBar.percent = dragon!!.health / 50
        }

        if (dragon?.isDead == true && !hasItDied) {
            hasItDied = true
            this.fakeFinish()
        }

        if (shouldSpawnPortalSoon && !hasPortalSpawned) {
            portalSpawnTicks++

            if (portalSpawnTicks >= 200) {
                val dragonFight = (DenimServer.endWorld as ServerWorld).enderDragonFight
                (dragonFight as EnderDragonFightAccessor).invokeGenerateEndPortal(true)

                hasPortalSpawned = true
            }
        }

        if (triggeredTrueBattle && DragonBattleManager.dragon == null) {
            triggerTicks++

            if (triggerTicks >= 73 && messageStage == 0) {
                sendToAllPlayers(Text.of("Oh, you fool..."))
                messageStage++
            } else if (triggerTicks >= 108 && messageStage == 1) {
                sendToAllPlayers(Text.of("Did you really think it was over?"))
                messageStage++
            } else if (triggerTicks >= 156 && messageStage == 2) {
                sendToAllPlayers(Text.of("In fact..."))
                messageStage++
            } else if (triggerTicks >= 186 && messageStage == 3) {
                sendToAllPlayers(Text.of("It has only..."))
                messageStage++
            } else if (triggerTicks >= 220 && messageStage == 4) {
                sendToAllPlayers(Text.of("just.."))
                messageStage++
            } else if (triggerTicks >= 240 && messageStage == 5) {
                sendToAllPlayers(Text.of("begun."))
                messageStage++
            } else if (triggerTicks >= 270 && messageStage == 6) {
                DenimServer.endWorld.players.forEach {
                    // Heal player and make them invulnerable temporarily.
                    // Making it a status effect so the player can't just cheat it.
                    it.addStatusEffect(StatusEffectInstance(StatusEffects.RESISTANCE, 4, 255))
                    it.addStatusEffect(StatusEffectInstance(StatusEffects.REGENERATION, 10, 255))

                    it.world.createExplosion(null, it.pos.x, it.pos.y, it.pos.z, 3F, World.ExplosionSourceType.NONE)
                    it.velocity = it.velocity.add(Vec3d.of(it.movementDirection.vector).multiply(-5.4).add(0.0, 5.7, 0.0))

                    (it as ServerPlayerEntity).networkHandler.sendPacket(EntityVelocityUpdateS2CPacket(it.id, it.velocity))
                }

                messageStage++
            }

            if (triggerTicks >= 370 && messageStage == 7) {
                DenimServer.endWorld.createExplosion(null, 0.5, 156.0, 0.5, 2F, World.ExplosionSourceType.NONE)

                val crystal = EndCrystalEntity(DenimServer.endWorld, 0.5, 156.0, 0.5)
                crystal.setShowBottom(false)

                DenimServer.endWorld.spawnEntity(crystal)

                this.respawningCrystal = crystal

                messageStage++
            }

            if (messageStage == 8 && triggerTicks % 4 == 0) {
                val iterator = upgradingTowers.iterator()

                while (iterator.hasNext()) {
                    val it = iterator.next()
                    if (it.hasNext) {
                        it.next()
                        unfinishedTowers.remove(it.spike)
                    }

                    if (it.isComplete) {
                        unfinishedTowers.remove(it.spike)
                    }
                }
            }

            if (messageStage == 8 && triggerTicks % 15 == 0) {
                if (unfinishedTowers.isEmpty() && upgradingTowers.all { it.isComplete }) {
                    messageStage = 9

                    DenimServer.endWorld.players.forEach {
                        it.teleport(-7.0, it.world.getTopY(Heightmap.Type.MOTION_BLOCKING, -7, 0).toDouble(), 0.0)
                        it.setNoGravity(false)
                    }

                    return
                }

                if (unfinishedTowers.isEmpty())
                    return

                val tower = unfinishedTowers[0]

                if (upgradedTowers.contains(tower))
                    return

                upgradedTowers.add(tower)
                upgradingTowers.add(SpikeStructureBuilder(DenimServer.endWorld as ServerWorld, tower))

                respawningCrystal!!.beamTarget = BlockPos(tower.centerX, tower.height + 1, tower.centerZ)

                DenimServer.endWorld.playSound(null, respawningCrystal!!.beamTarget, SoundEvents.ENTITY_GENERIC_EXTINGUISH_FIRE, SoundCategory.HOSTILE, 0.6F, 0.8F)
            }

            if (messageStage == 9) {
                respawningCrystal!!.beamTarget = BlockPos.ORIGIN
                respawningCrystal!!.teleport(0.5, respawningCrystal!!.y - 1, 0.5)
                upgradingTowers.forEach {
                    if (it.animate() && messageStage == 9) {
                        messageStage++
                    }
                }
            }

            if (messageStage == 10) {
                if (respawningCrystal == null)
                    return

                respawningCrystal!!.remove(Entity.RemovalReason.DISCARDED)
                respawningCrystal = null
                DragonBattleManager.start()
            }

            if (messageStage == 7 || messageStage == 8) {
                // Teleport everyone
                DenimServer.endWorld.players.forEach {
                    if (it.pos.y >= 90 && it.pos.y != 100.0) {
                        it.velocity = Vec3d.ZERO
                        it.setNoGravity(true)
                        it.teleport(it.pos.x, 100.0, it.pos.z)
                        it.addStatusEffect(StatusEffectInstance(StatusEffects.SLOW_FALLING, 2, 5))

                        return@forEach
                    }

                    if (it.pos.y < 100.0 && it.pos.y < 90 && triggerTicks >= 370 + (3 * 20)) {
                        it.velocity = Vec3d.ZERO
                        it.setNoGravity(true)
                        it.teleport(it.pos.x, 100.0, it.pos.z)
                        it.addStatusEffect(StatusEffectInstance(StatusEffects.SLOW_FALLING, 2, 5))
                    }
                }
            }
        }
    }

    private fun sendToAllPlayers(text: Text) {
        DenimServer.server.playerManager.playerList.forEach {
            it.sendMessage(text, false)
        }
    }
}