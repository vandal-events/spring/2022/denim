package events.vandal.denim.boss

import events.vandal.denim.Denim
import events.vandal.denim.boss.phase.FocusBeamPhaseManager
import events.vandal.denim.boss.phase.HomingSweepManager
import events.vandal.denim.enderoid.EnderoidEntity
import events.vandal.denim.enderoid.EnderoidManager
import events.vandal.denim.mixin.boss.EnderDragonFightAccessor
import events.vandal.denim.mixin.network.ServerPlayerEntityAccessor
import events.vandal.denim.server.DenimServer
import net.minecraft.block.Blocks
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityData
import net.minecraft.entity.EntityType
import net.minecraft.entity.SpawnReason
import net.minecraft.entity.boss.BossBar
import net.minecraft.entity.boss.ServerBossBar
import net.minecraft.entity.boss.dragon.EnderDragonEntity
import net.minecraft.entity.boss.dragon.phase.ChargingPlayerPhase
import net.minecraft.entity.boss.dragon.phase.PhaseType
import net.minecraft.entity.damage.DamageSource
import net.minecraft.entity.decoration.ArmorStandEntity
import net.minecraft.entity.decoration.EndCrystalEntity
import net.minecraft.network.packet.s2c.play.PlaySoundS2CPacket
import net.minecraft.network.packet.s2c.play.StopSoundS2CPacket
import net.minecraft.registry.entry.RegistryEntry
import net.minecraft.registry.tag.DamageTypeTags
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvent
import net.minecraft.sound.SoundEvents
import net.minecraft.text.*
import net.minecraft.util.Formatting
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.ChunkPos
import net.minecraft.util.math.MathHelper
import net.minecraft.util.math.Vec3d
import net.minecraft.world.GameRules
import net.minecraft.world.Heightmap
import net.minecraft.world.ServerWorldAccess
import net.minecraft.world.World
import net.minecraft.world.dimension.DimensionTypes
import net.minecraft.world.gen.feature.EndPortalFeature
import net.minecraft.world.gen.feature.EndSpikeFeature
import net.minecraft.world.gen.feature.FeatureConfig
import java.util.*
import kotlin.random.Random.Default.nextInt

object DragonBattleManager {
    // Music lengths, for looping
    //private const val END_SONG_LENGTH = 3761 // music/denim/andrius.ogg
    private const val PRE_END_SONG_LENGTH = 6883 // music/game/end/boss.ogg
    //private const val END2_SONG_LENGTH = 5446 // music/denim/andrius_end.ogg
    private const val END_SONG_LENGTH = 5440 // music/denim/end_at_hand.ogg

    const val MUSIC_VOLUME = 0.6F

    var dragon: EnderDragonEntity? = null
    var dragonData: EntityData? = null

    var isCinematic = false
    var isVisible = false
    var loadBossBar = false

    // Music handling
    var songStartedTick = mutableMapOf<ServerPlayerEntity, Int>()
    var preSongStartedTick = mutableMapOf<ServerPlayerEntity, Int>()
    var hasSongStarted = false
    var hasPlayedBefore = false

    // Homing dive stuff
    var lastHomingDivePhaseTime = 0
    var isRepeatingHomingDive = false
    var isHomingDive = false

    val crystals = mutableListOf<CustomEndCrystal>()

    val dragonBossBar = ServerBossBar(
        Text.translatable("entity.minecraft.ender_dragon"),
        BossBar.Color.PINK,
        BossBar.Style.PROGRESS
    ).setDragonMusic(true).setThickenFog(true) as ServerBossBar

    var animationTicks = 0
    /**
     * 0 - Start Phase
     * 1 - All Crystals Destroyed Phase
     * 2 - 50% Health Phase
     */
    var animationPhase = 0

    //var hasSecondSongStarted = false

    var doWeKnowItDied = false

    var stopped = false

    var startedTime = 0

    var howLongSinceStopped = 0

    fun initialize() {
        this.reset()

        dragon = EnderDragonEntity(EntityType.ENDER_DRAGON, DenimServer.endWorld)

        dragonBossBar.isVisible = true
        dragonBossBar.percent = 0.0F

        DenimServer.endWorld.players.forEach {
            dragonBossBar.addPlayer(it as ServerPlayerEntity)
        }

        // Kotlin shut the fuck up
        val dragonDeezNuts = dragon!!

        dragonData = dragonDeezNuts.initialize(
            DenimServer.endWorld as ServerWorldAccess,
            DenimServer.endWorld.getLocalDifficulty(
                DenimServer.endWorld.getTopPosition(Heightmap.Type.MOTION_BLOCKING, BlockPos(0, 15, 0))
            ),
            SpawnReason.TRIGGERED,
            null, null
        )
        dragonDeezNuts.setPos(0.0, -64.0, 0.0)
        dragonDeezNuts.isInvulnerable = true
        dragonDeezNuts.isInvisible = true
        dragonDeezNuts.isSilent = true
        dragonDeezNuts.isAiDisabled = false

        DenimServer.endWorld.spawnEntity(dragonDeezNuts)

        val dragonFight = (DenimServer.endWorld as ServerWorld).enderDragonFight
        if (dragonFight != null) {
            /*(dragonFight as EnderDragonFightAccessor).crystals.forEach {
                crystals.add(CustomEndCrystal(
                    it
                ))
            }*/

            val endWorld = DenimServer.endWorld
            for (spike in EndSpikeFeature.getSpikes(endWorld as ServerWorld)) {
                val list = endWorld.getNonSpectatingEntities(EndCrystalEntity::class.java, spike.boundingBox)
                crystals.addAll(list.map { CustomEndCrystal(it) })
            }
        }
    }

    fun tick() {
        if (stopped)
            return

        val currentTick = DenimServer.server.ticks

        if (DenimServer.endWorld.players.isEmpty()) {
            this.reset()

            return
        }

        if (dragon?.isDead == true || (dragon?.health != null && dragon?.health!! < 1F)) {
            if (!doWeKnowItDied) {
                doWeKnowItDied = true
                dragonBossBar.clearPlayers()
                val announceAdvancements = DenimServer.endWorld.gameRules.getBoolean(GameRules.ANNOUNCE_ADVANCEMENTS)

                DenimServer.endWorld.players.iterator().forEach {
                    (it as ServerPlayerEntity).advancementTracker.grantCriterion(
                        DenimServer.server.advancementLoader[Identifier(
                            "end/kill_dragon"
                        )], "impossible"
                    )
                    if (!announceAdvancements) {
                        DenimServer.server.sendMessage(
                            Text.translatable(
                                "chat.type.advancement.challenge",
                                it.displayName,
                                Text.translatable(
                                    "chat.square_brackets",
                                    Text.translatable("advancements.end.kill_dragon.title")
                                )
                                    .formatted(Formatting.DARK_PURPLE).styled { style ->
                                        style.withHoverEvent(
                                            HoverEvent(
                                                HoverEvent.Action.SHOW_TEXT,
                                                Text.translatable("advancements.end.kill_dragon.description")
                                            )
                                        )
                                    }
                            ))
                    }
                }

                playSound("entity.enderoid.defeat", null, SoundCategory.HOSTILE, Vec3d(0.0, 93.0, 0.0), 100000F)
            }

            // Code for all Enderoids to be pulled into the death of Jean
            if (doWeKnowItDied) {
                howLongSinceStopped++

                EnderoidManager.enderoids.iterator().forEach {
                    if (stopped)
                        return@forEach

                    val pullVector = Vec3d(0.0, 0.0, 0.0)
                        .add(dragon!!.pos)
                        .subtract(it.baseEntity.pos)
                        .multiply(5 / it.baseEntity.pos.distanceTo(dragon!!.pos))
                        .multiply(0.0575)

                    it.baseEntity.velocity = it.baseEntity.velocity.add(pullVector)
                }

                if (howLongSinceStopped >= 210) {
                    if (dragon?.isDead != true)
                        return

                    EnderoidManager.enderoids.toList().forEach {
                        it.baseEntity.remove(Entity.RemovalReason.KILLED)
                        EnderoidManager.removeEnderoid(it)
                    }
                }

                if (howLongSinceStopped >= 300) {
                    stopped = true

                    if (this.dragon != null) {
                        val fight = this.dragon!!.fight

                        if (fight != null) {
                            (fight as EnderDragonFightAccessor).invokeGenerateEndPortal(true)
                        } else {
                            val endPortalFeature = EndPortalFeature(true)

                            val world = DenimServer.endWorld as ServerWorld
                            var exitPortalLocation = world.getTopPosition(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, EndPortalFeature.offsetOrigin(BlockPos.ORIGIN))

                            while (world.getBlockState(exitPortalLocation).isOf(Blocks.BEDROCK) && exitPortalLocation.y > world.seaLevel) {
                                exitPortalLocation = exitPortalLocation.down()
                            }

                            if (endPortalFeature.generateIfValid(
                                    FeatureConfig.DEFAULT,
                                    world,
                                    world.chunkManager.chunkGenerator,
                                    net.minecraft.util.math.random.Random.create(),
                                    exitPortalLocation
                                )
                            ) {
                                val i = MathHelper.ceilDiv(4, 16)
                                world.chunkManager.threadedAnvilChunkStorage.forceLighting(
                                    ChunkPos(exitPortalLocation),
                                    i
                                )
                            }
                        }
                    }

                    return
                }
            }
        }

        if (loadBossBar) {
            dragonBossBar.percent += 0.02F

            if (dragonBossBar.percent >= 1F) {
                dragonBossBar.percent = 1F
                loadBossBar = false
            }
        }

        if (isCinematic) {
            animationTicks++

            if (animationPhase == 0) {
                if (animationTicks >= 40) {
                    isCinematic = false
                    animationTicks = 0

                    this.playSound("ender_dragon.initiate", null, SoundCategory.HOSTILE, dragon!!.pos, 100000.0F)
                    dragon!!.phaseManager.setPhase(PhaseType.HOLDING_PATTERN)
                }
            } else if (animationPhase == 1) {
                isCinematic = false
                runHomingSweep(true)
            } else if (animationPhase == 2) {
                isCinematic = false
                runHomingSweep(true)
            }
        }

        if (hasSongStarted) {
            if (startedTime < 60)
                startedTime++

            DenimServer.server.playerManager.playerList.forEach {
                if (it.world.registryKey.value == DimensionTypes.THE_END_ID) {
                    if (startedTime >= 60) {
                        if (
                            //this.crystals.isNotEmpty() &&
                            (songStartedTick[it] == null || currentTick - songStartedTick[it]!! >= END_SONG_LENGTH)
                        ) {
                            songStartedTick[it] = currentTick
                            stopSound("music.dragon", SoundCategory.RECORDS)

                            playSound(
                                //"music.denim.andrius",
                                "music.denim.end_at_hand",
                                it,
                                SoundCategory.RECORDS,
                                Vec3d.ZERO.add(0.0, 95.0, 0.0),
                                MUSIC_VOLUME,
                            )
                        }

                        if (preSongStartedTick.contains(it))
                            preSongStartedTick.remove(it)

                        /*if (
                            this.crystals.isEmpty() &&
                            ((songStartedTick[it] == null || currentTick - songStartedTick[it]!! >= END2_SONG_LENGTH) || !hasSecondSongStarted)
                        ) {
                            hasSecondSongStarted = true
                            songStartedTick[it] = currentTick
                            stopSound("music.dragon", SoundCategory.VOICE)

                            playSound(
                                "music.denim.andrius_end",
                                it,
                                SoundCategory.VOICE,
                                Vec3d.ZERO.add(0.0, 95.0, 0.0),
                                1000000.0F,
                            )
                        }*/
                    }
                }
            }
        } else {
            DenimServer.server.playerManager.playerList.forEach {
                if (it.world.registryKey.value == DimensionTypes.THE_END_ID) {
                    if (preSongStartedTick[it] == null || currentTick - preSongStartedTick[it]!! >= PRE_END_SONG_LENGTH) {
                        preSongStartedTick[it] = currentTick
                        it.playSound(
                            SoundEvents.MUSIC_DRAGON.value(),
                            SoundCategory.RECORDS,
                            1.0F,
                            1.0F
                        )
                    }
                }
            }
        }

        // Crystals Broken Phase
        if (isVisible && crystals.isEmpty() && animationPhase == 0) {
            dragon!!.isInvulnerable = false

            isCinematic = true
            animationPhase = 1
        }

        // Half health phase
        if (isVisible &&
            animationPhase == 1 &&
            dragon != null &&
            dragon!!.health <= dragon!!.maxHealth / 2
        ) {
            animationPhase = 2
            isCinematic = true
        }

        // Homing sweep
        if (dragon != null
            && dragon?.phaseManager?.current?.type == PhaseType.HOLDING_PATTERN
            && isVisible && !isCinematic
            && DenimServer.server.ticks - lastHomingDivePhaseTime >= 1500
            && nextInt(if (animationPhase == 0) 15 else if (animationPhase == 1) 10 else 5) == 0
            && DenimServer.server.ticks % 25 == 0
        ) {
            runHomingSweep()
        } else if (
            dragon != null && DenimServer.server.ticks - FocusBeamPhaseManager.lastFocusBeam >= 3600
            && isVisible && !isCinematic
            && nextInt(if (animationPhase == 0) 50 else if (animationPhase == 1) 35 else 15) == 0
            && DenimServer.server.ticks % 15 == 0
        ) {
            attemptFocusBeam()
        }

        crystals.toMutableList().forEach {
            it.tick()
        }
    }

    fun runHomingSweep(force: Boolean = false) {
        val players = DenimServer.endWorld.players.filter { !it.isCreative && !it.isSpectator && !it.isInvulnerable }

        if (players.isEmpty())
            return

        if (force) {
            lastHomingDivePhaseTime = 0
            isRepeatingHomingDive = true
        }

        isHomingDive = true
        dragon?.phaseManager?.setPhase(PhaseType.CHARGING_PLAYER)
        val current = (dragon?.phaseManager?.current) as ChargingPlayerPhase

        HomingSweepManager.selectedTarget = players.random() as ServerPlayerEntity
        current.pathTarget = HomingSweepManager.selectedTarget!!.pos.add(0.0, -2.0, 0.0)
    }

    fun start() {
        this.initialize()

        isVisible = true
        hasSongStarted = true

        dragon!!.phaseManager.setPhase(Denim.RISING_PHASE)

        crystals.forEach {
            it.endCrystal.world.createExplosion(null, it.endCrystal.pos.x, it.endCrystal.pos.y, it.endCrystal.pos.z, 3.0F, World.ExplosionSourceType.NONE)

            for (shieldId in 0..2) {
                val shield = EnderShield(shieldId, DenimServer.endWorld as ServerWorld, it.endCrystal.pos)
                it.shields.add(shield)
                shield.tick()
            }
        }

        /*DenimServer.endWorld.players.forEach {
            if (crystals.firstOrNull { crystal -> crystal.endCrystal.pos.distanceTo(it.pos) <= 15 } == null) return@forEach

            it.velocity = it.velocity.add(Vec3d.of(it.movementDirection.vector).multiply(-5.4).add(0.0, 2.7, 0.0))

            (it as ServerPlayerEntity).networkHandler.sendPacket(EntityVelocityUpdateS2CPacket(it.id, it.velocity))
        }*/
    }

    fun damageCrystal(crystal: EndCrystalEntity, source: DamageSource, amount: Float): Boolean {
        if (dragon != null && (dragon?.health ?: 0F) / (dragon?.maxHealth ?: 0F) != dragonBossBar.percent) {
            dragonBossBar.percent = dragon!!.health / dragon!!.maxHealth
        }

        if (source.isIn(DamageTypeTags.IS_EXPLOSION)) return false

        val foundCrystal = crystals.firstOrNull { it.endCrystal == crystal }
        val damageResult = foundCrystal?.damage(source, amount) ?: true

        if (damageResult) {
            foundCrystal?.bossBar?.isVisible = false
            crystals.remove(foundCrystal)
        }

        return damageResult
    }

    fun enableCinematic() {
        isCinematic = true
        loadBossBar = true

        println("enabled?")

        dragon!!.phaseManager.setPhase(PhaseType.SITTING_FLAMING)
    }

    fun reset() {
        stopped = false

        FocusBeamPhaseManager.sentTip = false

        hasSongStarted = false
        hasPlayedBefore = false
        dragonData = null
        preSongStartedTick.clear()
        songStartedTick.clear()

        startedTime = 0

        crystals.forEach {
            it.bossBar.clearPlayers()
            it.shields.forEach { shield ->
                shield.destroy()
            }
        }
        crystals.clear()

        isVisible = false
        isCinematic = false

        lastHomingDivePhaseTime = 0
        isRepeatingHomingDive = false
        animationPhase = 0
        animationTicks = 0

        dragonBossBar.isVisible = false
        dragonBossBar.clearPlayers()
        dragonBossBar.percent = 1.0F

        doWeKnowItDied = false
        hasPlayedBefore = false

        howLongSinceStopped = 0

        HomingSweepManager.end()

        dragon?.remove(Entity.RemovalReason.DISCARDED)
        dragon = null

        stopSound("music.denim.end_at_hand", SoundCategory.RECORDS)

        DenimServer.server.playerManager.playerList.forEach {
            (it as ServerPlayerEntityAccessor).seenCredits = false

            (it as ServerPlayerEntity).advancementTracker.revokeCriterion(
                DenimServer.server.advancementLoader[Identifier(
                    "end/kill_dragon"
                )], "impossible"
            )
        }
    }

    // These sound methods should definitely be their own class.
    // But oh well.
    fun stopSound(name: String, category: SoundCategory = SoundCategory.MASTER) {
        val stopSoundPacket = StopSoundS2CPacket(Identifier(name), category)

        DenimServer.endWorld.players.forEach {
            val serverPlayerEntity = it as ServerPlayerEntity
            serverPlayerEntity.networkHandler.sendPacket(stopSoundPacket)
        }
    }

    fun playSound(name: String, player: ServerPlayerEntity? = null, category: SoundCategory = SoundCategory.MASTER, pos: Vec3d, volume: Float = 1F, pitch: Float = 1F, world: World = DenimServer.endWorld) {
        val soundPacket = PlaySoundS2CPacket(RegistryEntry.of(SoundEvent.of(Identifier(name))), category, pos.x, pos.y, pos.z, volume, pitch, 0L)

        if (player == null) {
            world.players.forEach {
                val serverPlayer = it as ServerPlayerEntity
                serverPlayer.networkHandler.sendPacket(soundPacket)
            }
        } else {
            player.networkHandler.sendPacket(soundPacket)
        }
    }

    fun damageCrystalByShield(entity: ArmorStandEntity, source: DamageSource, amount: Float) {
        val crystal = crystals.firstOrNull { it.shields.firstOrNull { shield -> shield.entity.id == entity.id } != null } ?: return

        this.damageCrystal(crystal.endCrystal, source, amount)
    }

    fun spawnEnderoid(pos: Vec3d, world: World = DenimServer.endWorld): EnderoidEntity? {
        val ziglinEntity = EntityType.ZOMBIFIED_PIGLIN.create(world as ServerWorld, null, null, BlockPos.ofFloored(pos), SpawnReason.TRIGGERED, false, false)
            ?: return null

        val enderoid = EnderoidEntity(ziglinEntity)

        EnderoidManager.addEnderoid(enderoid)
        world.spawnEntityAndPassengers(ziglinEntity)

        return enderoid
    }

    fun sendFinishingMessage(player: ServerPlayerEntity) {
        player.sendMessage(
            Texts.join(listOf(
                Text.of("\n\n\n"),
                Text.of("---------------------------------\n").copy().formatted(Formatting.GOLD),
                Text.of("Congratulations on defeating the").copy().formatted(Formatting.WHITE),
                Text.of("Ender Dragon!\n\n").copy().formatted(Formatting.DARK_PURPLE),
                Text.of("Vandal Events").copy().formatted(Formatting.AQUA, Formatting.BOLD),
                Text.of("is far from over. We will be hosting more events like these in the near future, so join our").copy().formatted(Formatting.WHITE),
                Text.of("Discord").copy().styled {
                    it.withColor(TextColor.fromRgb(0x5865F2))
                        .withFormatting(Formatting.UNDERLINE)
                        .withHoverEvent(HoverEvent(HoverEvent.Action.SHOW_TEXT, Text.of("Join our Discord! https://discord.gg/Dvv6HFEunV")))
                        .withClickEvent(ClickEvent(ClickEvent.Action.OPEN_URL, "https://discord.gg/Dvv6HFEunV"))
                },
                Text.of("to stay updated!\n\n").copy().formatted(Formatting.WHITE),
                Text.of("---------------------------------\n").copy().formatted(Formatting.GOLD)
            ), Text.of(" ")), false)

        player.sendMessage(Text.of(""), false)

        player.sendMessage(
            Texts.join(listOf(
                Text.of("You can check your stats for this event using").copy().formatted(Formatting.WHITE),
                Text.of("/stats").copy().formatted(Formatting.YELLOW, Formatting.UNDERLINE).styled {
                    it.withClickEvent(ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/stats"))
                },
                Text.of(".\n\n"),
                Text.of("Calm End Music:").copy().formatted(Formatting.LIGHT_PURPLE, Formatting.BOLD),
                Text.of("C418 - Boss\n").copy().formatted(Formatting.GOLD).styled {
                    it
                        .withFormatting(Formatting.UNDERLINE)
                        .withHoverEvent(HoverEvent(HoverEvent.Action.SHOW_TEXT, Text.of("Clicking me sends you to a YouTube page of the song.")))
                        .withClickEvent(ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.youtube.com/watch?v=NBBxG9biq1Q"))
                },
                Text.of("Dragon Battle Music:").copy().formatted(Formatting.LIGHT_PURPLE, Formatting.BOLD),
                Text.of("Solunary - The End at Hand").copy().formatted(Formatting.GOLD).styled {
                    it
                        .withFormatting(Formatting.UNDERLINE)
                        .withHoverEvent(HoverEvent(HoverEvent.Action.SHOW_TEXT, Text.of("Clicking me sends you to a YouTube page of the song.")))
                        .withClickEvent(ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.youtube.com/watch?v=GAW5tuC83mE"))
                }
            ),
                Text.of(" ")
            ),
            false
        )
    }

    fun attemptFocusBeam(): Boolean {
        if (!FocusBeamPhaseManager.hasStarted &&
            !isHomingDive && dragon!!.phaseManager.current.type !== Denim.RISING_PHASE && dragon!!.phaseManager.current.type !== Denim.MOVE_TO_SPIKE_PHASE
        ) {
            if (DenimServer.server.ticks - FocusBeamPhaseManager.lastFocusBeam < 3600) // 1 minute
                return false

            // When the Dragon begins a new phase, there's a 1/15 chance of it being a focus beam.
            if (Random().nextInt(15) == 0) {
                //FocusBeamPhaseManager.start()
                dragon!!.phaseManager.setPhase(Denim.MOVE_TO_SPIKE_PHASE) // I am a fucking idiot.
                return true
            }
        }

        return false
    }
}