package events.vandal.denim.boss

import events.vandal.denim.boss.phase.HomingSweepManager
import events.vandal.denim.server.DenimServer
import net.minecraft.entity.boss.BossBar
import net.minecraft.entity.boss.ServerBossBar
import net.minecraft.entity.boss.dragon.phase.PhaseType
import net.minecraft.entity.damage.DamageSource
import net.minecraft.entity.decoration.EndCrystalEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.network.packet.s2c.play.EntityVelocityUpdateS2CPacket
import net.minecraft.particle.ParticleTypes
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvents
import net.minecraft.text.Text
import net.minecraft.util.math.Vec3d
import kotlin.math.absoluteValue
import kotlin.random.Random

data class CustomEndCrystal(
    val endCrystal: EndCrystalEntity,
    val shields: MutableList<EnderShield> = mutableListOf(),
    val health: MutableList<Float> = mutableListOf(50.0F, 50.0F, 50.0F, 150.0F)
) {
    private var isDamaged = false
    private var damageTime = 0
    private var lastAmbient = 0

    val bossBar: ServerBossBar = ServerBossBar(Text.of("End Crystal"), BossBar.Color.PURPLE, BossBar.Style.NOTCHED_6).apply {
        isVisible = true
        this.percent = 1F
    }

    fun damage(source: DamageSource, amount: Float): Boolean {
        if (isDamaged) return false

        if (health.isEmpty()) {
            DragonBattleManager.crystals.remove(this)
            DragonBattleManager.playSound("end_crystal.broken", null, SoundCategory.HOSTILE, endCrystal.pos, pitch = 0.4F)

            return true
        } // Unlikely, but it could be possible for it to somehow empty out

        health[0] -= amount
        if (shields.isNotEmpty()) {
            shields[0].damage(health[0] <= 0F)

            if (health[0] <= 0F) {
                if (source.attacker is ServerPlayerEntity) {
                    val player = source.attacker as ServerPlayerEntity

                    DragonBattleManager.spawnEnderoid(this.endCrystal.pos.add(0.0, 0.0, 1.575))?.immediatelyAnger(player)
                    DragonBattleManager.spawnEnderoid(this.endCrystal.pos.add(0.0, 0.0, -1.575))?.immediatelyAnger(player)
                    DragonBattleManager.spawnEnderoid(this.endCrystal.pos.add(1.575, 0.0, 0.0))?.immediatelyAnger(player)
                    DragonBattleManager.spawnEnderoid(this.endCrystal.pos.add(-1.575, 0.0, 0.0))?.immediatelyAnger(player)

                    /*for (i in 0..3) {
                        DragonBattleManager.spawnEnderoid(this.endCrystal.pos.add(0.0, 0.0, 1.575))?.immediatelyAnger(player)
                    }*/

                    if (player.distanceTo(endCrystal) <= 15) {
                        player.velocity = player.velocity.add(Vec3d.of(player.movementDirection.vector).multiply(-1.125))
                        player.networkHandler.sendPacket(EntityVelocityUpdateS2CPacket(player.id, player.velocity))
                    }
                }
            }
        }

        val copiedHealth = health[0]

        if (health[0] <= 0F) {
            health.removeAt(0)

            if (shields.isNotEmpty())
                shields.removeAt(0)

            if (HomingSweepManager.isInitialized) { // Cancel when the crystal is damaged
                DragonBattleManager.dragon?.phaseManager?.setPhase(PhaseType.HOLDING_PATTERN)
            }
        }

        if (copiedHealth < 0F) // Apply damage to the others, since otherwise it'll be depleting per bar
            return damage(source, copiedHealth.absoluteValue)

        isDamaged = true
        damageTime = DenimServer.server.ticks

        if (source.attacker is PlayerEntity && !bossBar.players.contains(source.attacker))
            bossBar.addPlayer(source.attacker as ServerPlayerEntity)

        bossBar.percent = try {
            health.reduce { a, b -> a + b }
        } catch (_: Exception) {
            0F
        } / 300F

        if (health.isEmpty()) {
            DragonBattleManager.crystals.remove(this)
            DragonBattleManager.playSound("end_crystal.broken", null, SoundCategory.HOSTILE, endCrystal.pos, pitch = 0.4F)

            return true
        } else {
            if (shields.isEmpty())
                DragonBattleManager.playSound("end_crystal.hurt", null, SoundCategory.HOSTILE, endCrystal.pos, pitch = 0.7F)
        }

        return false
    }

    var damagedTicks = 0

    fun tick() {
        shields.forEach {
            it.tick()
        }
        if (isDamaged && DenimServer.server.ticks - damageTime > 8) {
            isDamaged = false
        }

        if (shields.isNotEmpty() && Random.nextInt(10) == 0 && DenimServer.server.ticks - lastAmbient >= 200) {
            lastAmbient = DenimServer.server.ticks

            DragonBattleManager.playSound("crystal_shield.ambient", null, SoundCategory.HOSTILE, endCrystal.pos, 0.9F)
            //endCrystal.world.playSound(endCrystal.pos.x, endCrystal.pos.y, endCrystal.pos.z, Denim.SHIELD_AMBIENT, SoundCategory.HOSTILE, 0.4F, 1.0F, true)
        }

        if (shields.isEmpty() && damagedTicks++ % 5 == 0) {
            DenimServer.endWorld.playSound(null, endCrystal.blockPos, SoundEvents.ENTITY_GENERIC_EXTINGUISH_FIRE, SoundCategory.HOSTILE, 0.6F, 0.8F)
            DenimServer.endWorld.addParticle(ParticleTypes.ELECTRIC_SPARK,true, endCrystal.pos.x, endCrystal.pos.y, endCrystal.pos.z,.8,.8,.8)

            // Periodic and random self damage for being exposed.
            if (Random.nextInt(25) == 0) {
                this.endCrystal.damage(this.endCrystal.world.damageSources.generic(), 4.735F)
            }

            DenimServer.endWorld.players.forEach {
                if (it.distanceTo(endCrystal) <= 5.0) {
                    attack(it)
                }
            }
        }

        bossBar.players.filter { it.pos.distanceTo(endCrystal.pos) >= 120 }.forEach {
            bossBar.removePlayer(it)
        }
    }

    fun attack(player: PlayerEntity) {
        // Possible particles
        // /particle minecraft:electric_spark ^ ^ ^5 .2 .2 .2 0 15
        // /particle minecraft:enchant ^ ^ ^3 .1 .1 .1 .7 4 // Best?
        player.velocity = player.velocity.add(Vec3d.of(player.movementDirection.vector).multiply(-0.38))
        (player as ServerPlayerEntity).networkHandler.sendPacket(EntityVelocityUpdateS2CPacket(player.id, player.velocity))

        player.damage(player.world.damageSources.magic(), 0.7525F)
    }
}
