package events.vandal.denim.boss

import events.vandal.denim.Denim
import events.vandal.denim.server.DenimServer
import net.minecraft.enchantment.Enchantments
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityType
import net.minecraft.entity.EquipmentSlot
import net.minecraft.entity.decoration.ArmorStandEntity
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtInt
import net.minecraft.network.packet.s2c.play.EntityAttributesS2CPacket
import net.minecraft.particle.ParticleTypes
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvent
import net.minecraft.util.Identifier
import net.minecraft.util.math.EulerAngle
import net.minecraft.util.math.Vec3d
import kotlin.random.Random

class EnderShield(id: Int, val world: ServerWorld, val pos: Vec3d) {
    val entity = ArmorStandEntity(EntityType.ARMOR_STAND, world)
    private val isClockwise = Random.nextBoolean()
    private val speed = Random.nextDouble(0.4, 0.85).toFloat()
    private var isDamaged = false
    private var damageTime = 0

    init {
        entity.setPos(pos.x, pos.y - 1.0 + (id - 0.5), pos.z)
        entity.setNoGravity(true)
        entity.isInvisible = true
        entity.isInvulnerable = true
        entity.equipStack(EquipmentSlot.HEAD, ItemStack(Items.SHULKER_SHELL).apply {
            this.setSubNbt("CustomModelData", NbtInt.of(1))
            this.addEnchantment(Enchantments.PROTECTION, 1)
        })

        entity.setHeadYaw(Random.nextFloat() * 360F)

        world.spawnEntity(entity)
    }

    fun tick() {
        if (isClockwise)
            entity.headRotation = EulerAngle(entity.headRotation.pitch, entity.headRotation.yaw + speed, entity.headRotation.roll)
        else
            entity.headRotation = EulerAngle(entity.headRotation.pitch, entity.headRotation.yaw - speed, entity.headRotation.roll)

        if (isDamaged && DenimServer.server.ticks - damageTime >= 8) {
            isDamaged = false
            entity.equipStack(EquipmentSlot.HEAD, ItemStack(Items.SHULKER_SHELL).apply {
                this.setSubNbt("CustomModelData", NbtInt.of(1))
                this.addEnchantment(Enchantments.PROTECTION, 1)
            })
        }
    }

    fun damage(destroy: Boolean = false) {
        damageTime = DenimServer.server.ticks
        isDamaged = true

        if (destroy)
            return this.destroy()

        //world.playSound(pos.x, pos.y, pos.z, Denim.SHIELD_HURT, SoundCategory.HOSTILE, 0.6F, 1.0F, true)
        DragonBattleManager.playSound("crystal_shield.hurt", null, SoundCategory.HOSTILE, pos, 1.0F)
        entity.equipStack(EquipmentSlot.HEAD, ItemStack(Items.SHULKER_SHELL).apply {
            this.setSubNbt("CustomModelData", NbtInt.of(2))
            this.addEnchantment(Enchantments.PROTECTION, 1)
        })
    }

    fun destroy() {
        world.spawnParticles(ParticleTypes.CRIT, pos.x, pos.y, pos.z, 150, .4, .2, .4, .8)
        //world.playSound(pos.x, pos.y, pos.z, Denim.SHIELD_SHATTER, SoundCategory.HOSTILE, 0.6F, 1.0F, true)
        DragonBattleManager.playSound("crystal_shield.broken", null, SoundCategory.HOSTILE, pos, 0.9F)
        entity.remove(Entity.RemovalReason.DISCARDED)
    }
}