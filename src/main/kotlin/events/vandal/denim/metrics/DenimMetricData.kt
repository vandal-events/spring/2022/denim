package events.vandal.denim.metrics

import com.google.gson.JsonObject
import com.mojang.authlib.GameProfile
import events.vandal.metrics.MetricData

data class DenimMetricData(
    val player: GameProfile,
    var crystalsDestroyed: Int = 0,
    var totalDragonDamage: Float = 0F,
    var deaths: Int = 0,
    var enderManKills: Int = 0,
    var totalBucketPlaces: Int = 0,
    var totalArrowsShot: Int = 0,
    var totalDamageTaken: Double = 0.0,
    var totalCrystalDamage: Float = 0F,
    var enderoidsKnocked: Int = 0,
    var carpalTunnel: Int = 0
) : MetricData(player) {
    override fun serialize(): JsonObject {
        val data = super.serialize()
        return data.apply {
            addProperty("crystalsDestroyed", crystalsDestroyed)
            addProperty("totalDragonDamage", totalDragonDamage)
            addProperty("deaths", deaths)
            addProperty("enderManKills", enderManKills)
            addProperty("totalBucketPlaces", totalBucketPlaces)
            addProperty("totalArrowsShot", totalArrowsShot)
            addProperty("totalDamageTaken", totalDamageTaken)
            addProperty("totalCrystalDamage", totalCrystalDamage)
            addProperty("enderoidsKnocked", enderoidsKnocked)
            addProperty("carpalTunnel", carpalTunnel)
        }
    }

    companion object {
        operator fun invoke(
            player: GameProfile,
            crystalsDestroyed: Int? = 0,
            totalDragonDamage: Float? = 0F,
            deaths: Int? = 0,
            enderManKills: Int? = 0,
            totalBucketPlaces: Int? = 0,
            totalArrowsShot: Int? = 0,
            totalDamageTaken: Double? = 0.0,
            totalCrystalDamage: Float? = 0F,
            enderoidsKnocked: Int? = 0,
            carpalTunnel: Int? = 0
        ) = DenimMetricData(
            player,
            crystalsDestroyed ?: 0,
            totalDragonDamage ?: 0F,
            deaths ?: 0,
            enderManKills ?: 0,
            totalBucketPlaces ?: 0,
            totalArrowsShot ?: 0,
            totalDamageTaken ?: 0.0,
            totalCrystalDamage ?: 0F,
            enderoidsKnocked ?: 0,
            carpalTunnel ?: 0
        )

        fun deserialize(data: JsonObject): DenimMetricData {
            val profile = MetricData.deserialize(data)

            return DenimMetricData(
                profile,
                getOrNull(data, "crystalsDestroyed")?.asInt,
                getOrNull(data, "totalDragonDamage")?.asFloat,
                getOrNull(data, "deaths")?.asInt,
                getOrNull(data, "enderManKills")?.asInt,
                getOrNull(data, "totalBucketPlaces")?.asInt,
                getOrNull(data, "totalArrowsShot")?.asInt,
                getOrNull(data, "totalDamageTaken")?.asDouble,
                getOrNull(data, "totalCrystalDamage")?.asFloat,
                getOrNull(data, "enderoidsKnocked")?.asInt,
                getOrNull(data, "carpalTunnel")?.asInt
            )
        }
    }
}
