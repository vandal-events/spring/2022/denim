package events.vandal.denim.mixin.network;

import events.vandal.denim.boss.DragonBattleManager;
import events.vandal.denim.boss.fake.FakeDragonBattleManager;
import events.vandal.denim.enderoid.EnderoidEntityDuck;
import events.vandal.denim.mixin.DamageTrackerAccessor;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.damage.DamageTracker;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Objects;

@Mixin(ServerPlayerEntity.class)
public class ServerPlayerEntityMixin {
    @Shadow public ServerPlayNetworkHandler networkHandler;

    @Inject(at = @At("RETURN"), method = "playerTick")
    public void addBossBarIfDied(CallbackInfo ci) {
        if (this.networkHandler == null)
            return;

        if (DragonBattleManager.INSTANCE.isVisible())
            DragonBattleManager.INSTANCE.getDragonBossBar().addPlayer((ServerPlayerEntity) (Object) this);
        else if (FakeDragonBattleManager.INSTANCE.isFake())
            FakeDragonBattleManager.INSTANCE.getDragonBossBar().addPlayer((ServerPlayerEntity) (Object) this);
    }

    @Redirect(at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/damage/DamageTracker;getDeathMessage()Lnet/minecraft/text/Text;"), method = "onDeath")
    public Text replaceDeathMessageWithDenim(DamageTracker instance) {
        var recentDamageList = ((DamageTrackerAccessor) instance).getRecentDamage();
        var record = recentDamageList.get(recentDamageList.size() - 1);
        var recentDamage = record.damageSource();
        var biggestAttacker = recentDamage.getAttacker();

        if (biggestAttacker == null) {
            return instance.getDeathMessage();
        }

        if (!biggestAttacker.isLiving())
            return instance.getDeathMessage();

        if (Objects.requireNonNull(recentDamage.getAttacker()).getType().equals(EntityType.ZOMBIFIED_PIGLIN) || Objects.requireNonNull(biggestAttacker).getType().equals(EntityType.ZOMBIFIED_PIGLIN)) {
            var ziglinEntity = recentDamage.getAttacker().getType().equals(EntityType.ZOMBIFIED_PIGLIN) ? recentDamage.getAttacker() : biggestAttacker;

            // This entity is most definitely an Enderoid
            if (ziglinEntity instanceof EnderoidEntityDuck && ((EnderoidEntityDuck) ziglinEntity).isEnderoid()) {
                var enderoidEntity = ((EnderoidEntityDuck) ziglinEntity).getEnderoidEntity();
                var player = (ServerPlayerEntity) (Object) this;
                enderoidEntity.endPlayerMount(player.getPos());

                if (recentDamage.getAttacker().equals(ziglinEntity)) {
                    if (!enderoidEntity.isMounted()) {
                        return Text.translatable("death.attack.enderoidSwipe", player.getDisplayName(), ziglinEntity.getName());
                    } else {
                        return Text.translatable("death.attack.enderoidMount", player.getDisplayName(), ziglinEntity.getName());
                    }
                } else {
                    if (!enderoidEntity.isMounted())
                        return Text.translatable("death.attack.enderoidSwipe.player", player.getDisplayName(), Objects.requireNonNull(ziglinEntity.getName()));
                    else
                        return Text.translatable("death.attack.enderoidMount.player", player.getDisplayName(), Objects.requireNonNull(ziglinEntity.getName()));
                }
            }
        }

        return instance.getDeathMessage();
    }
}
