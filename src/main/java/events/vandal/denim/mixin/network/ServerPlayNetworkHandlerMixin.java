package events.vandal.denim.mixin.network;

import events.vandal.denim.Denim;
import events.vandal.denim.boss.DragonBattleManager;
import events.vandal.denim.enderoid.EnderoidManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.packet.c2s.play.ClientStatusC2SPacket;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Objects;

@Mixin(ServerPlayNetworkHandler.class)
public abstract class ServerPlayNetworkHandlerMixin {
    @Shadow public ServerPlayerEntity player;

    /*@Inject(at = @At(value = "INVOKE", target = "Lnet/minecraft/server/network/ServerPlayerEntity;setStackInHand(Lnet/minecraft/util/Hand;Lnet/minecraft/item/ItemStack;)V", ordinal = 0), method = "onPlayerAction", cancellable = true, locals = LocalCapture.CAPTURE_FAILHARD)
    public void preventEnderoidSwap(PlayerActionC2SPacket packet, CallbackInfo ci, BlockPos blockPos, PlayerActionC2SPacket.Action action, ItemStack itemStack) {
        ci.cancel();
    }*/

    @Shadow public abstract ServerPlayerEntity getPlayer();

    @Redirect(at = @At(value = "INVOKE", target = "Lnet/minecraft/screen/ScreenHandler;onSlotClick(IILnet/minecraft/screen/slot/SlotActionType;Lnet/minecraft/entity/player/PlayerEntity;)V"), method = "onClickSlot")
    public void stopMovingSlot(ScreenHandler instance, int slotIndex, int button, SlotActionType actionType, PlayerEntity player) {
        var slot = instance.getSlot(slotIndex);
        if (slot.hasStack() && slot.getStack().hasNbt() && Objects.requireNonNull(slot.getStack().getNbt()).containsUuid("enderoidUuid")) {
            try {
                var enderoidUuid = slot.getStack().getNbt().getUuid("enderoidUuid");
                if (EnderoidManager.INSTANCE.getEnderoids().stream().noneMatch(entity -> entity.getUuid().equals(enderoidUuid))) {
                    instance.onSlotClick(slotIndex, button, actionType, player);
                    return;
                }

                var enderoid = EnderoidManager.INSTANCE.getEnderoids().stream().filter(entity -> entity.getUuid().equals(enderoidUuid)).toList().get(0);

                if (actionType == SlotActionType.THROW) {
                    if (!enderoid.isMounted()) {
                        Denim.Companion.getLogger().warn("Player {} apparently has the Enderoid mounted, but the entity reports that it's not mounted! Deleting object.", player.getDisplayName().getString());
                        player.getInventory().remove(it -> it.hasNbt() && Objects.requireNonNull(it.getNbt()).containsUuid("enderoidUuid") && it.getNbt().getUuid("enderoidUuid").equals(enderoid.getUuid()), 1, player.getInventory());

                        return;
                    }

                    enderoid.attemptDrop();
                }

                return;
            } catch (Exception e) {
                instance.onSlotClick(slotIndex, button, actionType, player);
                return;
            }
        }

        instance.onSlotClick(slotIndex, button, actionType, player);
    }

    @Inject(at = @At(value = "INVOKE", target = "Lnet/minecraft/advancement/criterion/ChangedDimensionCriterion;trigger(Lnet/minecraft/server/network/ServerPlayerEntity;Lnet/minecraft/registry/RegistryKey;Lnet/minecraft/registry/RegistryKey;)V"), method = "onClientStatus")
    public void sendFinishingMessage(ClientStatusC2SPacket packet, CallbackInfo ci) {
        DragonBattleManager.INSTANCE.sendFinishingMessage(this.getPlayer());
    }

    @Redirect(at = @At(value = "INVOKE", target = "Lnet/minecraft/server/PlayerManager;respawnPlayer(Lnet/minecraft/server/network/ServerPlayerEntity;Z)Lnet/minecraft/server/network/ServerPlayerEntity;"), method = "onClientStatus")
    public ServerPlayerEntity removeEnderoidIfStillMounted(PlayerManager instance, ServerPlayerEntity player, boolean alive) {
        var newPlayer = instance.respawnPlayer(player, alive);

        // In case the player didn't get the Enderoid off of their head, it should be by respawn time.
        newPlayer.getInventory().remove((itemStack ->
            itemStack.hasNbt() && Objects.requireNonNull(itemStack.getNbt()).containsUuid("enderoidUuid")
        ), 1, newPlayer.getInventory());

        return newPlayer;
    }
}
