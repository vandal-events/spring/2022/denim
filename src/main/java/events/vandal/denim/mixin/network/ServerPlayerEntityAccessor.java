package events.vandal.denim.mixin.network;

import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(ServerPlayerEntity.class)
public interface ServerPlayerEntityAccessor {
    @Accessor("seenCredits")
    void setSeenCredits(boolean seenCredits);

    @Accessor
    boolean getSeenCredits();
}
