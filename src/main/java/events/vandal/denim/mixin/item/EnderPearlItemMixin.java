package events.vandal.denim.mixin.item;

import events.vandal.denim.boss.DragonBattleManager;
import events.vandal.denim.boss.fake.FakeDragonBattleManager;
import events.vandal.denim.server.DenimServer;
import net.minecraft.entity.decoration.EndCrystalEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.EnderPearlItem;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.EndSpikeFeature;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(EnderPearlItem.class)
public class EnderPearlItemMixin {
    @Inject(at = @At("HEAD"), method = "use", cancellable = true)
    public void preventPearl(World world, PlayerEntity user, Hand hand, CallbackInfoReturnable<TypedActionResult<ItemStack>> cir) {
        // Prevent the pearl from being used if all the towers that aren't Pearl Towers are still alive
        if (FakeDragonBattleManager.INSTANCE.isFake())
            return;

        var isUsable = true;
        var spikes = EndSpikeFeature.getSpikes((ServerWorld) DenimServer.endWorld);

        for (EndSpikeFeature.Spike spike : spikes) {
            // 82, 103
            if (spike.getHeight() == 82 || spike.getHeight() == 103)
                continue;

            var list = DenimServer.endWorld.getNonSpectatingEntities(EndCrystalEntity.class, spike.getBoundingBox());
            if (!list.isEmpty()) {
                isUsable = false;
                break;
            }
        }

        if (!isUsable) {
            user.sendMessage(Text.of("[!] The energy of the crystals appear to be preventing you from using Ender Pearls.").copy().formatted(Formatting.RED), false);
            user.getItemCooldownManager().set((EnderPearlItem) (Object) this, 20);
            cir.setReturnValue(TypedActionResult.fail(user.getStackInHand(hand)));
        }
    }
}
