package events.vandal.denim.mixin.block;

import events.vandal.denim.boss.fake.FakeDragonBattleManager;
import net.minecraft.block.BlockState;
import net.minecraft.block.EndPortalBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.function.BooleanBiFunction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(EndPortalBlock.class)
public class EndPortalBlockMixin {
    @Inject(at = @At("HEAD"), method = "onEntityCollision", cancellable = true)
    public void launchIfFake(BlockState state, World world, BlockPos pos, Entity entity, CallbackInfo ci) {
        if (world instanceof ServerWorld && !entity.hasVehicle() && !entity.hasPassengers() && entity.canUsePortals() && VoxelShapes.matchesAnywhere(VoxelShapes.cuboid(entity.getBoundingBox().offset(-pos.getX(), -pos.getY(), -pos.getZ())), state.getOutlineShape(world, pos), BooleanBiFunction.AND)) {
            if (entity.getType() != EntityType.PLAYER)
                return;

            if (FakeDragonBattleManager.INSTANCE.isFake() && !FakeDragonBattleManager.INSTANCE.getTriggeredTrueBattle()) {
                FakeDragonBattleManager.INSTANCE.endFake();

                ci.cancel();
            }
        }
    }
}
