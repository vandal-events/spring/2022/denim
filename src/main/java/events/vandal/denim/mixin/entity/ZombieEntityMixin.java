package events.vandal.denim.mixin.entity;

import events.vandal.denim.boss.DragonBattleManager;
import events.vandal.denim.enderoid.EnderoidEntity;
import events.vandal.denim.enderoid.EnderoidEntityDuck;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.entity.mob.ZombifiedPiglinEntity;
import net.minecraft.sound.SoundCategory;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ZombieEntity.class)
class ZombieEntityMixin {
    @Inject(at = @At("TAIL"), method = "damage", cancellable = true)
    public void enderoidDamageSound(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        if (((ZombieEntity) (Object) this) instanceof ZombifiedPiglinEntity && ((EnderoidEntityDuck) this).isEnderoid()) {
            var entity = ((ZombieEntity) (Object) this);

            if (entity.isDead())
                DragonBattleManager.INSTANCE.playSound("entity.enderoid.death", null, SoundCategory.HOSTILE, entity.getPos(), EnderoidEntity.VOLUME, 1F, entity.getEntityWorld());
            else {
                DragonBattleManager.INSTANCE.playSound("entity.enderoid.hurt", null, SoundCategory.HOSTILE, entity.getPos(), EnderoidEntity.VOLUME, 1F, entity.getEntityWorld());
                ((EnderoidEntityDuck) this).getEnderoidEntity().damage(source);
            }
        }
    }

    @Inject(at = @At("HEAD"), method = "damage", cancellable = true)
    public void preventEnderFeuds(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        if (source.getAttacker() == null)
            return;

        var livingEntity = (LivingEntity) (Object) this;

        if (livingEntity == null)
            return;

        if (((ZombieEntity) (Object) this) instanceof ZombifiedPiglinEntity && ((EnderoidEntityDuck) this).isEnderoid()) {
            if (source.getAttacker().getType() == EntityType.ENDER_DRAGON || source.getAttacker().getType() == EntityType.ENDERMAN) {
                cir.cancel();
            }
        } else if (livingEntity.getType() == EntityType.ENDER_DRAGON) {
            if (source.getAttacker() == null)
                return;

            if (source.getAttacker().getType() == EntityType.ENDERMAN ||
                    (source.getAttacker() != null && source.getAttacker().getType() == EntityType.ZOMBIFIED_PIGLIN && ((EnderoidEntityDuck) source.getAttacker()).isEnderoid())
            ) {
                cir.cancel();
            }
        } else if (livingEntity.getType() == EntityType.ENDERMAN) {
            // tbh it's funnier if the dragon kills these guys
            if ((source.getAttacker() != null && source.getAttacker().getType() == EntityType.ZOMBIFIED_PIGLIN && ((EnderoidEntityDuck) source.getAttacker()).isEnderoid())) {
                cir.cancel();
            }
        }
    }
}