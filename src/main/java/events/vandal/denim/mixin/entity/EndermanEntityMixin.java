package events.vandal.denim.mixin.entity;

import events.vandal.denim.Denim;
import events.vandal.denim.metrics.DenimMetricData;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.EndermanEntity;
import net.minecraft.entity.player.PlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(EndermanEntity.class)
public class EndermanEntityMixin {
    @Inject(at = @At("TAIL"), method = "damage")
    public void damage(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        if (((LivingEntity) (Object) this).getHealth() <= 0F) {
            if (source.getAttacker() instanceof PlayerEntity) {
                DenimMetricData metrics = Denim.Companion.getMetrics().getMetric(((PlayerEntity) source.getAttacker()).getGameProfile());
                metrics.setEnderManKills(metrics.getEnderManKills() + 1);
            }
        }
    }
}   
