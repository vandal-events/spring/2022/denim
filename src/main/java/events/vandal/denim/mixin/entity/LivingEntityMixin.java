package events.vandal.denim.mixin.entity;

import events.vandal.denim.enderoid.EnderoidEntityDuck;
import events.vandal.denim.enderoid.EnderoidManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(LivingEntity.class)
public class LivingEntityMixin {
    @Inject(method = "getLootTable", at = @At("HEAD"), cancellable = true)
    public void dropEnderoidItems(CallbackInfoReturnable<Identifier> cir) {
        if (((LivingEntity) (Object) this).getType() == EntityType.ZOMBIFIED_PIGLIN && ((EnderoidEntityDuck) this).isEnderoid()) {
            cir.cancel();

            cir.setReturnValue(new Identifier("denim", "entities/enderoid"));

            if (EnderoidManager.INSTANCE.getEnderoids().stream().anyMatch(entity -> entity.getUuid().equals(((EnderoidEntityDuck) this).getEnderoidEntity().getUuid()))) {
                EnderoidManager.INSTANCE.removeEnderoid(((EnderoidEntityDuck) this).getEnderoidEntity());
            }
        }
    }

    @Redirect(method = "onDeath", at = @At(value = "INVOKE", target = "Lorg/slf4j/Logger;info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V", remap = false))
    public void ignoreDeathInConsole(org.slf4j.Logger instance, String s, Object o, Object p) {
        if (((Entity) (Object) this).getType() == EntityType.ZOMBIFIED_PIGLIN && ((EnderoidEntityDuck) this).isEnderoid())
            return;

        instance.info(s, o, p);
    }
}
