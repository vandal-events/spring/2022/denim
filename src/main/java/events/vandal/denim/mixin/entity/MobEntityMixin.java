package events.vandal.denim.mixin.entity;

import events.vandal.denim.boss.DragonBattleManager;
import events.vandal.denim.enderoid.EnderoidEntity;
import events.vandal.denim.enderoid.EnderoidEntityDuck;
import events.vandal.denim.enderoid.EnderoidManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.ZombifiedPiglinEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.util.collection.DefaultedList;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(MobEntity.class)
public abstract class MobEntityMixin {
    @Unique private boolean confirmedNotEnderoid = false;

    @Inject(at = @At("HEAD"), method = "baseTick")
    public void baseTick(CallbackInfo ci) {
        if (((MobEntity)(Object) this).getType() == EntityType.ZOMBIFIED_PIGLIN) {
            if (((EnderoidEntityDuck) this).isEnderoid()) {
                ((EnderoidEntityDuck) this).getEnderoidEntity().tick();
            } else if (!confirmedNotEnderoid) {
                // Double check that the entity is in fact an Enderoid,
                // because it tends to unload, causing the Enderoid system to stop working.
                LivingEntity livingEntity = (LivingEntity) (Object) this;
                var stack = livingEntity.getEquippedStack(EquipmentSlot.HEAD);

                if (stack.getNbt() != null && stack.getNbt().containsUuid("enderoidUuid")) {
                    var enderoidDuck = ((EnderoidEntityDuck) this);
                    enderoidDuck.setEnderoid(true);

                    enderoidDuck.setEnderoidEntity(EnderoidManager.INSTANCE.recreateEnderoid((ZombifiedPiglinEntity) (Object) this, stack.getNbt().getUuid("enderoidUuid")));
                } else {
                    confirmedNotEnderoid = true;
                }
            }
        }
    }

    @Inject(at = @At("TAIL"), method = "tryAttack")
    public void tryAttack(Entity target, CallbackInfoReturnable<Boolean> cir) {
        if (((MobEntity)(Object) this).getType() == EntityType.ZOMBIFIED_PIGLIN) {
            if (((EnderoidEntityDuck) this).isEnderoid()) {
                DragonBattleManager.INSTANCE.playSound(
                        "entity.enderoid.slash",
                        null,
                        SoundCategory.HOSTILE,
                        ((Entity) (Object) this).getPos(),
                        EnderoidEntity.VOLUME,
                        1F,
                        ((Entity) (Object) this).getWorld()
                );
            }
        }
    }
}
