package events.vandal.denim.mixin.boss;

import net.minecraft.block.pattern.BlockPattern;
import net.minecraft.entity.boss.dragon.EnderDragonFight;
import net.minecraft.entity.decoration.EndCrystalEntity;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.gen.Invoker;

import java.util.List;

@Mixin(EnderDragonFight.class)
public interface EnderDragonFightAccessor {
    @Accessor("crystals")
    List<EndCrystalEntity> getCrystals();

    @Invoker("generateEndPortal")
    void invokeGenerateEndPortal(boolean previouslyKilled);
}
