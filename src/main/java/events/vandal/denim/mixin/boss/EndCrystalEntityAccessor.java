package events.vandal.denim.mixin.boss;

import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.decoration.EndCrystalEntity;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.util.Optional;

@Mixin(EndCrystalEntity.class)
public interface EndCrystalEntityAccessor {
    @Accessor("BEAM_TARGET")
    static TrackedData<Optional<BlockPos>> getBeamTarget() {
        throw new AssertionError();
    }
}
