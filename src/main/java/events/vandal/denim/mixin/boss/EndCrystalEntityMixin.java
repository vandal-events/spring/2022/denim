package events.vandal.denim.mixin.boss;

import events.vandal.denim.Denim;
import events.vandal.denim.boss.DragonBattleManager;
import events.vandal.denim.boss.fake.FakeDragonBattleManager;
import events.vandal.denim.metrics.DenimMetricData;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.boss.dragon.EnderDragonEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.decoration.EndCrystalEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.registry.tag.DamageTypeTags;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Objects;

@Mixin(EndCrystalEntity.class)
public abstract class EndCrystalEntityMixin {
    @Shadow protected abstract void crystalDestroyed(DamageSource source);

    @SuppressWarnings("UnreachableCode")
    @Inject(at = @At(value = "HEAD"), method = "damage", cancellable = true)
    public void damage(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        if (FakeDragonBattleManager.INSTANCE.isFake())
            return;

        if (FakeDragonBattleManager.INSTANCE.getTriggeredTrueBattle() && !DragonBattleManager.INSTANCE.isVisible()) {
            cir.cancel();
            return;
        }

        cir.cancel();

        // Why the FUCK does this work
        Entity entity = (Entity) (Object) this;

        if (entity.isInvulnerableTo(source) || source.getAttacker() instanceof EnderDragonEntity) {
            cir.setReturnValue(false);
            return;
        }

        if (source.isIn(DamageTypeTags.IS_EXPLOSION)) {
            cir.setReturnValue(false);
            return;
        }

        if (!entity.isRemoved() && !entity.getWorld().isClient) {
            boolean damageResult = DragonBattleManager.INSTANCE.damageCrystal((EndCrystalEntity) (Object) this, source, amount);

            if (source.getAttacker() != null && Objects.requireNonNull(source.getAttacker()).getType() == EntityType.PLAYER) {
                DenimMetricData metrics = Denim.Companion.getMetrics().getMetric(((PlayerEntity) source.getAttacker()).getGameProfile());
                metrics.setTotalCrystalDamage(metrics.getTotalCrystalDamage() + amount);
            }

            if (damageResult) {
                entity.remove(Entity.RemovalReason.KILLED);
                if (!source.isIn(DamageTypeTags.IS_EXPLOSION)) {
                    entity.getWorld().createExplosion(null, entity.getX(), entity.getY(), entity.getZ(), 9.0f, World.ExplosionSourceType.NONE);
                }
                this.crystalDestroyed(source);

                if (source.getAttacker() != null && Objects.requireNonNull(source.getAttacker()).getType() == EntityType.PLAYER) {
                    DenimMetricData metrics = Denim.Companion.getMetrics().getMetric(((PlayerEntity) source.getAttacker()).getGameProfile());
                    metrics.setCrystalsDestroyed(metrics.getCrystalsDestroyed() + 1);
                }

                cir.setReturnValue(true);
            } else {
                cir.setReturnValue(false);
            }
            return;
        }
        cir.setReturnValue(false);
    }
}
