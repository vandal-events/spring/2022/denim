package events.vandal.denim.mixin.boss.phase;

import net.minecraft.entity.boss.dragon.phase.Phase;
import net.minecraft.entity.boss.dragon.phase.PhaseType;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(PhaseType.class)
public interface PhaseTypeAccessor<T> {
    @Invoker("register")
    static <T extends Phase> PhaseType<T> register(Class<T> phaseClass, String name) {
        throw new AssertionError();
    }
}
