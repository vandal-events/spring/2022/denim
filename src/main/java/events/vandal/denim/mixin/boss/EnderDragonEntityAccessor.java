package events.vandal.denim.mixin.boss;

import net.minecraft.entity.boss.dragon.EnderDragonEntity;
import net.minecraft.entity.boss.dragon.EnderDragonPart;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(EnderDragonEntity.class)
public interface EnderDragonEntityAccessor {
    @Accessor
    EnderDragonPart getNeck();

    @Accessor
    EnderDragonPart getLeftWing();

    @Accessor
    EnderDragonPart getRightWing();
}
