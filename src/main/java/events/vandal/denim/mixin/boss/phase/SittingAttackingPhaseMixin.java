package events.vandal.denim.mixin.boss.phase;

import events.vandal.denim.boss.phase.FocusBeamPhaseManager;
import net.minecraft.entity.boss.dragon.EnderDragonEntity;
import net.minecraft.entity.boss.dragon.phase.AbstractSittingPhase;
import net.minecraft.entity.boss.dragon.phase.PhaseType;
import net.minecraft.entity.boss.dragon.phase.SittingAttackingPhase;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(SittingAttackingPhase.class)
public class SittingAttackingPhaseMixin {
    @Inject(at = @At("HEAD"), method = "serverTick", cancellable = true)
    public void serverTick(CallbackInfo ci) {
        if (FocusBeamPhaseManager.INSTANCE.getHasStarted()) {
            FocusBeamPhaseManager.INSTANCE.tick();
            ci.cancel();
        }
    }

    @Inject(at = @At("HEAD"), method = "beginPhase", cancellable = true)
    public void beginPhase(CallbackInfo ci) {
        if (FocusBeamPhaseManager.INSTANCE.getHasStarted()) {
            FocusBeamPhaseManager.INSTANCE.start();
            ci.cancel();
        }
    }
}