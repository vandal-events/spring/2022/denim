package events.vandal.denim.mixin.boss.phase;

import com.mojang.brigadier.exceptions.CommandSyntaxException;
import events.vandal.denim.boss.DragonBattleManager;
import events.vandal.denim.enderoid.EnderoidEntity;
import events.vandal.denim.mixin.other.SpreadPlayersCommandInvoker;
import net.minecraft.entity.boss.dragon.phase.SittingScanningPhase;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.mob.ZombifiedPiglinEntity;
import net.minecraft.server.command.SpreadPlayersCommand;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.Vec2f;
import net.minecraft.world.Heightmap;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Mixin(SittingScanningPhase.class)
public class SittingScanningPhaseMixin {
    @Shadow private int ticks;

    @Inject(at = @At("HEAD"), method = "serverTick")
    public void spawnEnderoids(CallbackInfo ci) {
        // Too much work to get something that is most likely to just be one.
        var dragon = DragonBattleManager.INSTANCE.getDragon();

        if (dragon == null)
            return;

        if (this.ticks % 20 == 0 && new Random().nextInt(35) == 0) {
            // Dear god if 11 Enderoids spawn I'd feel bad
            var enderoidSpawnCount = new Random().nextInt(1, 4);
            //if (enderoidSpawnCount <= 0) return;

            assert dragon != null;

            //final double maxRange = 32.0;
            //Random random = new Random();

            // I don't know how Java arrays work
            ArrayList<ZombifiedPiglinEntity> entities = new ArrayList<>();

            //var center = new Vec2f((float) dragon.getPos().getX(), (float) dragon.getPos().getZ());

            var random = new Random();

            for (int i = 0; i < enderoidSpawnCount; i++) {
                EnderoidEntity enderoid = DragonBattleManager.INSTANCE.spawnEnderoid(dragon.getPos().add(random.nextDouble(-10.0, 10.0), 3.0, random.nextDouble(-10.0, 10.0)), dragon.getWorld());
                if (enderoid == null)
                    continue;

                enderoid.getBaseEntity().addStatusEffect(new StatusEffectInstance(StatusEffects.RESISTANCE, 3, 255, false, false));

                entities.add(enderoid.getBaseEntity());
            }

            // This is literally copied from the SpreadPlayersCommand code.
            /*double d = center.x - maxRange;
            double e = center.y - maxRange;
            double f = center.x + maxRange;
            double g = center.y + maxRange;
            SpreadPlayersCommand.Pile[] piles = SpreadPlayersCommandInvoker.invokeMakePiles(random, entities.size(), d, e, f, g);

            Pattern pattern = Pattern.compile("spread of at most (\\d+)");

            try {
                SpreadPlayersCommandInvoker.invokeSpread(center, 0.475, (ServerWorld) dragon.getWorld(), random, d, e, f, g, 36, piles, false);
            } catch (Exception cse) {
                try {
                    var matcher = pattern.matcher(cse.getMessage());

                    // we don't need the value for it, because we know it only actually occurs once
                    matcher.find();
                    var someHackyBullshit = matcher.group(1);

                    SpreadPlayersCommandInvoker.invokeSpread(center, Double.parseDouble(someHackyBullshit), (ServerWorld) dragon.getWorld(), random, d, e, f, g, 36, piles, false);
                } catch (Exception ignored) {}
            }

            // You can println this if you're curious towards the min distance
            SpreadPlayersCommandInvoker.invokeGetMinDistance(entities, (ServerWorld) dragon.getWorld(), piles, 36, false);*/

            entities.forEach(entity -> {
                double x = random.nextDouble(-10.0, 10.0);
                double z = random.nextDouble(-10.0, 10.0);
                int y = dragon.getWorld().getTopY(Heightmap.Type.MOTION_BLOCKING, (int) x, (int) z);

                entity.teleport(x, y + 1, z);
            });
        }
    }
}
