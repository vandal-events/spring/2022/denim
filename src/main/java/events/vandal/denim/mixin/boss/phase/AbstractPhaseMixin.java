package events.vandal.denim.mixin.boss.phase;

import events.vandal.denim.Denim;
import events.vandal.denim.boss.DragonBattleManager;
import events.vandal.denim.boss.fake.FakeDragonBattleManager;
import events.vandal.denim.boss.phase.FocusBeamPhaseManager;
import events.vandal.denim.server.DenimServer;
import net.minecraft.entity.boss.dragon.EnderDragonEntity;
import net.minecraft.entity.boss.dragon.phase.AbstractPhase;
import net.minecraft.entity.boss.dragon.phase.Phase;
import net.minecraft.entity.boss.dragon.phase.PhaseType;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Random;

@Mixin(AbstractPhase.class)
public class AbstractPhaseMixin {
    @Shadow @Final protected EnderDragonEntity dragon;

    @Inject(at = @At("HEAD"), method = "endPhase", cancellable = true)
    public void endPhase(CallbackInfo ci) {
        if (((Phase) this).getType() == PhaseType.SITTING_ATTACKING && FocusBeamPhaseManager.INSTANCE.getHasStarted()) {
            FocusBeamPhaseManager.INSTANCE.reset();
            ci.cancel();
        }
    }

    @Inject(at = @At("HEAD"), method = "beginPhase", cancellable = true)
    public void beginPhase(CallbackInfo ci) {
        if (FakeDragonBattleManager.INSTANCE.isFake()) return;

        if (DragonBattleManager.INSTANCE.attemptFocusBeam())
            ci.cancel();
    }
}
