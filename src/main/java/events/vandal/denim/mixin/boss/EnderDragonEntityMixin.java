package events.vandal.denim.mixin.boss;

import events.vandal.denim.Denim;
import events.vandal.denim.boss.DragonBattleManager;
import events.vandal.denim.boss.fake.FakeDragonBattleManager;
import events.vandal.denim.enderoid.EnderoidManager;
import events.vandal.denim.metrics.DenimMetricData;
import events.vandal.denim.boss.phase.HomingSweepManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.boss.dragon.EnderDragonEntity;
import net.minecraft.entity.boss.dragon.EnderDragonPart;
import net.minecraft.entity.boss.dragon.phase.PhaseManager;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.Box;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.List;
import java.util.Objects;

@Mixin(EnderDragonEntity.class)
public abstract class EnderDragonEntityMixin {
    @Shadow @Final private PhaseManager phaseManager;

    @Shadow public abstract PhaseManager getPhaseManager();

    @Shadow protected abstract boolean parentDamage(DamageSource source, float amount);

    @Inject(at = @At("HEAD"), method = "damage", cancellable = true)
    public void damage(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        if (FakeDragonBattleManager.INSTANCE.isFake()) {
            this.parentDamage(source, amount / 2);
            FakeDragonBattleManager.INSTANCE.getDragonBossBar().setPercent(((LivingEntity) (Object) this).getHealth() / 50);

            cir.setReturnValue(false);

            return;
        }

        if (((LivingEntity) (Object) this).isInvulnerable() || DragonBattleManager.INSTANCE.isCinematic() || !DragonBattleManager.INSTANCE.getCrystals().isEmpty()) {
            cir.cancel();
            cir.setReturnValue(false);
        }
    }

    @Inject(at = @At("HEAD"), method = "damagePart", cancellable = true)
    public void preventDamagePart(EnderDragonPart part, DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        if (FakeDragonBattleManager.INSTANCE.isFake()) {
            this.parentDamage(source, amount / 2);
            FakeDragonBattleManager.INSTANCE.getDragonBossBar().setPercent(((LivingEntity) (Object) this).getHealth() / 50);

            cir.setReturnValue(false);

            return;
        }

        if (((LivingEntity) (Object) this).isInvulnerable() || DragonBattleManager.INSTANCE.isCinematic() || !DragonBattleManager.INSTANCE.getCrystals().isEmpty()) {
            cir.cancel();
            cir.setReturnValue(false);
        }
    }

    @Inject(at = @At("RETURN"), method = "damagePart")
    public void damagePart(EnderDragonPart part, DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        if (cir.getReturnValue()) {
            if (FakeDragonBattleManager.INSTANCE.isFake()) {
                FakeDragonBattleManager.INSTANCE.getDragonBossBar().setPercent(((LivingEntity) (Object) this).getHealth() / 50);

                return;
            }

            DragonBattleManager.INSTANCE.getDragonBossBar().setPercent(((LivingEntity) (Object) this).getHealth() / ((LivingEntity) (Object) this).getMaxHealth());

            if (Objects.requireNonNull(source.getAttacker()).getType() == EntityType.PLAYER) {
                DenimMetricData metrics = Denim.Companion.getMetrics().getMetric(((PlayerEntity) source.getAttacker()).getGameProfile());
                metrics.setTotalDragonDamage(metrics.getTotalDragonDamage() + amount);
            }
        }
    }

    @ModifyVariable(at = @At("HEAD"), method = "damageLivingEntities", argsOnly = true)
    public List<Entity> removeDragonAlliesFromDamage(List<Entity> entities) {
        return removeEntities(entities);
    }

    @ModifyVariable(at = @At("HEAD"), method = "launchLivingEntities", argsOnly = true)
    public List<Entity> removeDragonAlliesFromLaunch(List<Entity> entities) {
        return removeEntities(entities);
    }

    private List<Entity> removeEntities(List<Entity> entities) {
        entities.removeIf((entity) -> EnderoidManager.INSTANCE.getEnderoids().stream().anyMatch((enderoid) -> enderoid.getBaseEntity().getUuid().equals(entity.getUuid())));
        entities.removeIf((entity) -> entity.getType() == EntityType.SHULKER);

        return entities;
    }

    @Inject(at = @At("HEAD"), method = "destroyBlocks", cancellable = true)
    public void preventDestroyingBlocks(Box box, CallbackInfoReturnable<Boolean> cir) {
        // Far too lazy to implement a proper thing, so this works
        cir.cancel();
    }

    @Inject(at = @At("HEAD"), method = "damageLivingEntities")
    public void damageLivingEntities(List<Entity> entities, CallbackInfo ci) {
        if (DragonBattleManager.INSTANCE.isHomingDive()) {
            entities.forEach(entity -> {
                if (!(entity instanceof LivingEntity))
                    return;

                HomingSweepManager.INSTANCE.damage((LivingEntity) entity);
            });
        }
    }

    @Redirect(at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/boss/dragon/EnderDragonEntity;setHealth(F)V"), method = "tickWithEndCrystals")
    public void preventOverhealWhenFake(EnderDragonEntity instance, float v) {
        if (FakeDragonBattleManager.INSTANCE.isFake()) {
            if (v <= 50F) {
                instance.setHealth(v);
            }

            FakeDragonBattleManager.INSTANCE.getDragonBossBar().setPercent(((LivingEntity) (Object) this).getHealth() / 50);
        } else {
            instance.setHealth(v);
        }
    }
}
