package events.vandal.denim.mixin.other;

import net.minecraft.entity.Entity;
import net.minecraft.server.command.SpreadPlayersCommand;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.Vec2f;
import net.minecraft.util.math.random.Random;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import java.util.Collection;

@Mixin(SpreadPlayersCommand.class)
public interface SpreadPlayersCommandInvoker {
    @Invoker("makePiles")
    static SpreadPlayersCommand.Pile[] invokeMakePiles(Random random, int count, double minX, double minZ, double maxX, double maxZ) {
        throw new AssertionError();
    }

    @Invoker("spread")
    static void invokeSpread(Vec2f center, double spreadDistance, ServerWorld world, net.minecraft.util.math.random.Random random, double minX, double minZ, double maxX, double maxZ, int maxY, SpreadPlayersCommand.Pile[] piles, boolean respectTeams) {
        throw new AssertionError();
    }

    @Invoker("getMinDistance")
    static double invokeGetMinDistance(Collection<? extends Entity> entities, ServerWorld world, SpreadPlayersCommand.Pile[] piles, int maxY, boolean respectTeams) {
        throw new AssertionError();
    }
}
