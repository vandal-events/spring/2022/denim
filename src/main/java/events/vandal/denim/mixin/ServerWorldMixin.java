package events.vandal.denim.mixin;

import events.vandal.denim.enderoid.EnderoidManager;
import net.minecraft.entity.Entity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.world.chunk.WorldChunk;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.stream.Stream;

@Mixin(ServerWorld.class)
public class ServerWorldMixin {
    @Inject(at = @At("TAIL"), method = "loadEntities")
    public void reloadEntities(Stream<Entity> entities, CallbackInfo ci) {
        EnderoidManager.INSTANCE.initWorld((ServerWorld) (Object) this);
    }
}
